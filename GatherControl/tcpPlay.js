function getTotalCallCMD(){
	return ",0";
}

//将一个数字转化成16进制字符串形式
function toHex(num){
	if(num<16){
		return "000"+num.toString(16).toUpperCase();
	}else if(num<256){
		return "00"+num.toString(16).toUpperCase();
	}else if(num<4096){
		return "0"+num.toString(16).toUpperCase();
	}else{
		return num.toString(16).toUpperCase();
	}
}
//将一个byte转化成16进制字符串形式
function byteToHex(num){
	if(num<16){
		return "0"+num.toString(16).toUpperCase();
	}else{
		return num.toString(16).toUpperCase();
	}
}

//低位在前，高位在后
function intToBytes( value )   
{   
	return byteToHex(((value) & 0xFF))
		+byteToHex(((value>>8) & 0xFF))
		+byteToHex(((value>>16) & 0xFF))
		+byteToHex(((value>>24) & 0xFF));  
}  

//高位在前，低位在后
function intToBytes2( value)   
{   
    return byteToHex(((value>>24) & 0xFF))
		+byteToHex(((value>>16) & 0xFF))
		+byteToHex(((value>>8) & 0xFF))
		+byteToHex(((value) & 0xFF));   
}

function getDownControl(exetype,ptype,addr,val,allYXState){
	print("exetype:"+exetype
	+" ptype:"+ptype
	+" addr:"+addr
	+" val:"+val
	+" allYXState:"+allYXState);
	
	var controlCmd = "NULL";
	//1[1 脚本解析协议,接收或转发第三方,2 程序默认协议,转发监听服务 3 脚本解析协议,tcp采集]
	var check = 0;
	
	if(1==exetype){
		controlCmd = "";
		check = 0;
	}else{
		if(1==ptype){
			controlCmd = "F30C00"
			+intToBytes(addr)
			+intToBytes(val)
			+"FF";
			check = 2;
		}else if(2==ptype){
			controlCmd = "F40C00"+toHex(addr)+toHex(val)+"FF";
			check = 2;
		}else{
			;
		}
	}
	return (controlCmd+","+check.toString(10));
}

function getReciveIDVAL(up_cmd,down_cmd){
	var up_cmd_ = up_cmd.toUpperCase();
	//print("up_cmd:"+up_cmd_);
	//print("down_cmd:"+down_cmd);
	if(1==(up_cmd_.length%2))
	{
		print("up_cmd length is not right.");
		return "";
	}
	if(up_cmd_.length<24)
	{
		print("up_cmd length is less than right.");
		return "";
	}
	var index01 = up_cmd_.indexOf("F3");
	var index02 = up_cmd_.indexOf("F4");
	if(0!=index01 && 0!=index02){
		print("up_cmd func is not right.");
		return "";
	}
	var lLen = up_cmd_.substr(2,2);
	var hLen = up_cmd_.substr(4,2);
	var len = parseInt("0X"+lLen)+256*parseInt("0X"+hLen);
	if(len<=0){
		print("up_cmd data len is not right.");
		return "";
	}
	var id04 = up_cmd_.substr( 6, 2 )
	var id03 = up_cmd_.substr( 8, 2 )
	var id02 = up_cmd_.substr( 10,2 )
	var id01 = up_cmd_.substr( 12,2 )
	var id = parseInt("0X"+id04)+256*parseInt("0X"+id03)+65536*parseInt("0X"+id02)+16777216*parseInt("0X"+id01);
	
	var val04 = up_cmd_.substr( 14, 2 )
	var val03 = up_cmd_.substr( 16, 2 )
	var val02 = up_cmd_.substr( 18,2 )
	var val01 = up_cmd_.substr( 20,2 )
	var val = parseInt("0X"+val04)+256*parseInt("0X"+val03)+65536*parseInt("0X"+val02)+16777216*parseInt("0X"+val01);
	return (id+","+val);
}