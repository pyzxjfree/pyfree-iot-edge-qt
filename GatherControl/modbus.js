﻿function getTotalCallCMD(){
	return "01030000000A,1";
}

//将一个数字转化成16进制字符串形式
function toHex(num){
	if(num<16){
		return "000"+num.toString(16).toUpperCase();
	}else if(num<256){
		return "00"+num.toString(16).toUpperCase();
	}else if(num<4096){
		return "0"+num.toString(16).toUpperCase();
	}else{
		return num.toString(16).toUpperCase();
	}
	//return num<16?"0"+num.toString(16).toUpperCase():num.toString(16).toUpperCase();
}

function getDownControl(exetype,ptype,addr,val,allYXState){
	print("exetype:"+exetype
	+" ptype:"+ptype
	+" addr:"+addr
	+" val:"+val
	+" allYXState:"+allYXState);
	
	var controlCmd = "NULL";
	//1[1 脚本解析协议,接收或转发第三方,2 程序默认协议,转发监听服务 3 脚本解析协议,tcp采集]
	var check = 0;
	
	if(1==exetype){
		controlCmd = "01030000000AC5CD";
		check = 0;
	}else{
		if(1==ptype||2==ptype){
			controlCmd = "0106"+toHex(addr)+toHex(val);
			check = 1;
			//controlCmd = "0106"..string.format("%04X",addr)..string.format("%04X",val);
		}else{
			;
		}
	}
	return (controlCmd+","+check.toString(10));
}

function getReciveIDVAL(up_cmd,down_cmd){
	//print("up_cmd:"+up_cmd);
	//print("down_cmd:"+down_cmd);
	if(1==(up_cmd.length%2))
	{
		print("up_cmd length is not right.");
		return "";
	}
	if(up_cmd.length<=46)
	{
		if(16==up_cmd.length
			&&0!=up_cmd.indexOf("0106"))
		{
			var idHStr = up_cmd.substr(4,2);
			var idLStr = up_cmd.substr(6,2);
			var id = 256*parseInt("0X"+idHStr)+parseInt("0X"+idLStr);
			var valHStr = up_cmd.substr(8,2);
			var valLStr = up_cmd.substr(10,2);
			var val = 256*parseInt("0X"+valHStr)+parseInt("0X"+valLStr);
			var ret_desc+=(id+","+val);
			return ret_desc;
		}else{
			print("up_cmd length is less than right.");
			return "";
		}	
	}
	var index = up_cmd.indexOf("0103");
	if(0!=index){
		print("up_cmd func is not right.");
		return "";
	}
	//print("index:"+index);
	var lLen = up_cmd.substr(4,2);
	//print("lLen:"+lLen);
	var dlen = parseInt("0X"+lLen);
	//print("dlen:"+dlen);
	if(dlen<0){
		print("up_cmd data length is error.");
		return "";
	}
	var ret_desc="";
	var id = 0;
	var size = (dlen/2);
	for(var i=6;i<(6+2*dlen);i+=4)
	{
		var valHStr = up_cmd.substr(i,2);
		var valLStr = up_cmd.substr(i+2,2);
		var val = 256*parseInt("0X"+valHStr)+parseInt("0X"+valLStr);
		//print("val:"+val);
		ret_desc+=(id+","+val);
		if(id<(size-1)){
			ret_desc+=";";
		}
		id+=1;
	}
	return ret_desc;
}
