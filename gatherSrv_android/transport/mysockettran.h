#ifndef MYSOCKETTRAN_H
#define MYSOCKETTRAN_H

#include <QThread>
#include <QAbstractSocket>
#include <QQueue>
#include <QMutex>

QT_BEGIN_NAMESPACE
class QTcpSocket;
class MyScript;
class ReceiveData;
QT_END_NAMESPACE

#include "data/datadef.h"
#include "config/confdef.h"

class MySocketTran : public QThread
{
    Q_OBJECT
public:
    MySocketTran(int _tranid
                 ,NetArg _netarg
                 ,ProtocolDef _protodefs
                 ,QObject *parent = Q_NULLPTR);
    ~MySocketTran();

    void stopRunning();
    //
    int getRDSize();
    bool isEmptyRD();
    bool getFirstRD(RD &it);

    bool removeFirstRD();
    void addRD(RD it);
    //
    int getWDSSize();
    bool isEmptyWDS();
    bool getFirstWDS(WDS &it);

    bool removeFirstWDS();
    void addWDS(WDS it);
protected:
    virtual void run();

private:
    bool initTcpSocket();
    bool destroyTcpSocket();
    bool connectMyHost();
    bool disconnectMyHost();
    void checklink();
    //
    int AddFrame(const unsigned char *buf, int len);
    int AddFrameDef(const unsigned char *buf, int len);
    int AddFrameTran(const unsigned char *buf, int len);
    void dataUncode();
    int getBuffer(unsigned char * _buf);
    int getBufferDef(unsigned char * buf);
    int getBufferTran(unsigned char * buf);
    void writeData();
    //
    qint64 getSec();
    qint64 getMSec();
signals:

private slots:
    void sockeError(QAbstractSocket::SocketError error);
    void readData();
private:
    bool running;
    int tranid;
    bool linking;
    bool ativity;
    qint64 linkTime;
    qint64 heartTime;
    NetArg netarg;
    ProtocolDef protodefs;
//    MySocketTranThread *ptr_MySocketTranThread;
    ReceiveData *m_WDData;
    QTcpSocket *client;
    //读取服务端数据缓存
    QQueue<RD> ReadData;
    QMutex m_MutexRD;
    int _ReadData_over;
    //待写入服务端数据缓存
    QQueue<WDS> WriteDataS;
    QMutex m_MutexWDS;
    bool OnDoIt_WDS;
    int _WriteData_overS;
    //
    MyScript *myScript;
    //
    RDClient rdc_data;
    //
    static int QSize;
};

#endif // MYSOCKETTRAN_H
