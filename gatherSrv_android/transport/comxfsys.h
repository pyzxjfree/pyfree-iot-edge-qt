#ifndef COMXFSYS_H
#define COMXFSYS_H

#include <QThread>
#include <QAbstractSocket>
#include <QQueue>
#include <QMutex>
#include <QString>

QT_BEGIN_NAMESPACE
class MyCacheData;
class ReceiveData;
class QTcpSocket;
QT_END_NAMESPACE

class ComXFSys : public QThread
{
    Q_OBJECT
public:
    ComXFSys(QObject *parent = Q_NULLPTR);
    virtual ~ComXFSys();
protected:
    virtual void run();
private:
    bool initTcpSocket();
    bool destroyTcpSocket();
    bool connectMyHost();
    bool disconnectMyHost();
    void checklink();
    void checkForReadyRead();
    //
    void dataUncode();
    void initWriteData();
    void writeData();
    void control(long areaId_,long devID, long pID, float val);
    //
    qint64 getSec();
    qint64 getMSec();
    //
    int getRDSize();
    bool isEmptyRD();
    bool getFirstRD(QString &it);

    bool removeFirstRD();
    void addRD(QString it);
signals:

private slots:
    void sockeError(QAbstractSocket::SocketError error);
    void readData();
private:
    bool running;
    long areaId;
    int  areaType;
    QString areaName;
    QString areaDesc;
    MyCacheData *ptr_MyCacheData;
    ReceiveData *ptr_ReceiveData;
    //
    QString ip;
    int port;
    QTcpSocket *client;
    bool linking;
    bool ativity;
    bool initDataFlag;
    qint64 linkTime;
    qint64 heartTime;
    //
    //读取服务端数据缓存
    QQueue<QString> ReadData;
    QMutex m_MutexRD;
    int _ReadData_over;
    //
    static int QSize;
};
#endif // COMXFSYS_H
