#include "mysockettran.h"

#include <QTcpSocket>
#include <QByteArray>
#include <QStringList>
#include <QDateTime>
#include <QDebug>

#include "script/MyScript.h"
#include "data/myfunc.h"
#include "data/receivedata.h"

//
int  MySocketTran::QSize = 100;

MySocketTran::MySocketTran(int _tranid
                           ,NetArg _netarg
                           ,ProtocolDef _protodefs
                           ,QObject *parent)
    : QThread(parent)
    , running(true)
    , tranid(_tranid)
    , linking(false)
    , ativity(false)
    , netarg(_netarg)
    , protodefs(_protodefs)
    , m_WDData(ReceiveData::getInstance())
    , client(NULL)
    , OnDoIt_WDS(false)
    , _ReadData_over(0)
    , _WriteData_overS(0)
    , myScript(NULL)
{
    linkTime = getSec();
    heartTime = getSec();
    if(1==netarg.ptype){
        myScript = new MyScript(protodefs.jsfile);
    }
};

MySocketTran::~MySocketTran()
{
    stopRunning();
    try{
        this->terminate();
        this->wait();
        qInfo() << "MySocketTran::destroy";
    }catch(...){
        qDebug() << "MySocketTran::destroy fail";
    }
};

void MySocketTran::stopRunning()
{
    running = false;
    qInfo() << "MySocketTran::stopRunning";
}

void MySocketTran::run()
{
    initTcpSocket();
    qRegisterMetaType<QAbstractSocket::SocketError>("MySocketError");
    qInfo() << "MySocketTran::run and initTcpSocket";
    while (running) {
        this->checklink();
        this->dataUncode();
        this->writeData();
        this->msleep(10);
    }
    try{
        destroyTcpSocket();
        qInfo() << "MySocketTran::destroyTcpSocket";
    }catch(...){
        qDebug() << "MySocketTran::destroyTcpSocket fail";
    }
    exec();
};

bool MySocketTran::initTcpSocket()
{
    try{
        if(NULL==client){
            client = new QTcpSocket();
            connect(client,SIGNAL(readyRead()),this,SLOT(readData()));
//            connect(client, &QTcpSocket::disconnected, client, &QTcpSocket::deleteLater);
            connect(client,SIGNAL(error(QAbstractSocket::SocketError))
                    ,this,SLOT(sockeError(QAbstractSocket::SocketError)));
            return true;
        }
        return false;
    }catch(...){
        qDebug() << "MySocketTran::initTcpSocket Exception";
        return false;
    }
}

bool MySocketTran::destroyTcpSocket()
{
    try{
        if(NULL!=client){
            disconnect(client,SIGNAL(readyRead()),this,SLOT(readData()));
//            disconnect(client, &QTcpSocket::disconnected, client, &QTcpSocket::deleteLater);
            disconnect(client,SIGNAL(error(QAbstractSocket::SocketError))
                    ,this,SLOT(sockeError(QAbstractSocket::SocketError)));
            linking = !disconnectMyHost();
            delete client;
            client = NULL;
        }
    }catch(...){
        qDebug() << "MySocketTran::destroyTcpSocket() Exception";
        return false;
    }
    return true;
}

bool MySocketTran::connectMyHost()
{
    try{
        if(NULL==client){
            initTcpSocket();
            return false;
        }
        if(QAbstractSocket::UnconnectedState==client->state()){
            client->connectToHost(netarg.ipStr,netarg.port,QIODevice::ReadWrite);
            if(client->waitForConnected(3000)){
                qInfo() << QString("MySocketTran,success connect to host(%1:%2)")
                            .arg(netarg.ipStr).arg(netarg.port);
                ativity = true;
                return true;
            }else{
                qWarning() << QString("MySocketTran,connect to host(%1:%2) fail!")
                            .arg(netarg.ipStr).arg(netarg.port);
                return false;
            }
        }else if(QAbstractSocket::ConnectedState==client->state()){
            return true;
        }else{
            return false;
        }
    }catch(...){
        qDebug() << "MySocketTran::connectMyHost Exception";
        return false;
    }
}

bool MySocketTran::disconnectMyHost()
{
    try{
        if(NULL==client){
            return true;
        }
        if(QAbstractSocket::ConnectedState==client->state())
        {
            qInfo() << QString("disconnect to host(%1:%2)")
                        .arg(netarg.ipStr).arg(netarg.port);
            client->disconnectFromHost();
            ativity = false;
            return client->waitForDisconnected();
        }else if(QAbstractSocket::UnconnectedState==client->state()){
            return true;
        }else{
            return false;
        }
    }catch(...){
        qDebug() << "MySocketTran::disconnectMyHost Exception";
        return true;
    }
}

void MySocketTran::checklink()
{
    try{
        if(!linking){
            if(linkTime< getSec()){
                linking = connectMyHost();
                linkTime = getSec()+30;
            }
        }else{
            if((heartTime+30)<getSec())
            {
                if(!ativity){
                    linking = !disconnectMyHost();
                }
                heartTime = getSec();
                ativity = false;
            }
        }
    }catch(...){
        qDebug() << "MySocketTran::checklink Exception";
    }
}

void MySocketTran::dataUncode()
{
    RD rddata;
    switch(netarg.type){
    case 1:
    {
        try {
            //数据帧解析
            if (this->getFirstRD(rddata))
            {
                unsigned char * pBuf = new unsigned char[rddata.len];
                int nLen = PFunc::uncode(rddata.Buf, rddata.len, pBuf);//反序列化处理
                this->AddFrame(pBuf, nLen);
                delete[] pBuf;
                pBuf = NULL;
                this->removeFirstRD();
            }
        }
        catch (...) {
            qDebug() << QString("Exception for Parsing Error,NetType(%1)!")
                        .arg(netarg.type);
        }
    }
        break;
    case 2:
    {
        try {
            //数据帧解析
            if (this->getFirstRD(rddata))
            {
                this->AddFrame(rddata.Buf, rddata.len);
                this->removeFirstRD();
            }
        }
        catch (...) {
            qDebug() << QString("Exception for Parsing Error,NetType(%1)!")
                        .arg(netarg.type);
        }
    }
        break;
    default:
        break;
    }
}

void MySocketTran::readData()
{
    if(NULL==client)
        return;
    if(QAbstractSocket::ConnectedState!=client->state()){
        return;
    }
    qInfo() << "MySocketTran::reatData be call";
    switch(netarg.type){
    case 1:
    {
        try {
            //直接读取,不用做分帧处理,ACSII字段
            char buf[256] = { 0 };
            int len = client->read(buf, 256);
            if (len > 0) {
                ativity = true;
                rdc_data.add((unsigned char*)buf, len);
//                RD rdata((unsigned char*)buf, len);
//                this->addRD(rdata);
                printf("read(%d) from pcs_server\n",len);
                unsigned char * buff = rdc_data.Buf;
                int start = 0;
                unsigned char ctype = 0;
                for (int i = 0; i < rdc_data.len; i++)
                {
                    printf("%02X ",buff[i]);
                    if (buff[i] > 0xf0) {
                        if (buff[i] == 0xff) {
                            if (ctype) {
                                ctype = 0;
                                RD rdata(buff + start, i - start + 1);
                                this->addRD(rdata);
                                start = i + 1;
                            }
                        }
                        else {
                            ctype = buff[i];
                            start = i;
                        }
                    }
                }
                printf("\n");
                buff = NULL;
                if (start < rdc_data.len)
                {
                    RDClient _newrd(rdc_data.Buf + start, rdc_data.len - start);
                    rdc_data = _newrd;
                }
                else {
                    rdc_data.len = 0;
                }
            }
        }
        catch (...) {
            qDebug() << QString("Exception for Reading Error,NetType(%1)!")
                        .arg(netarg.type);
        }
    }
        break;
    case 2:
    {
        try {
            //直接读取,不用做分帧处理,ACSII字段
            char buf[256] = { 0 };
            int len = client->read(buf, 256);
            if (len > 0) {
                ativity = true;
                printf("read(%d) from pcs_server\n",len);
                RD rdata((unsigned char*)buf, len);
                this->addRD(rdata);
            }
        }
        catch (...) {
            qDebug() << QString("Exception for Reading Error,NetType(%1)!")
                        .arg(netarg.type);
        }
    }
        break;
    default:
        break;
    }
}

void MySocketTran::sockeError(QAbstractSocket::SocketError error)
{
    if(NULL==client)
        return;
    switch (error) {
    case QAbstractSocket::ConnectionRefusedError:
        break;
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        break;
    case QAbstractSocket::SocketAccessError:
        break;
    case QAbstractSocket::SocketResourceError:
        break;
    case QAbstractSocket::SocketTimeoutError:
        return;
    case QAbstractSocket::DatagramTooLargeError:
        break;
    case QAbstractSocket::NetworkError:
        break;
    case QAbstractSocket::AddressInUseError:
        break;
    case QAbstractSocket::SocketAddressNotAvailableError:
        break;
    case QAbstractSocket::UnsupportedSocketOperationError:
        break;
    case QAbstractSocket::ProxyAuthenticationRequiredError:
        break;
    case QAbstractSocket::SslHandshakeFailedError:
        break;
    case QAbstractSocket::UnfinishedSocketOperationError:
        break;
    case QAbstractSocket::ProxyConnectionRefusedError:
        break;
    case QAbstractSocket::ProxyConnectionClosedError:
        break;
    case QAbstractSocket::ProxyConnectionTimeoutError:
        break;
    case QAbstractSocket::ProxyNotFoundError:
        break;
    case QAbstractSocket::ProxyProtocolError:
        break;
    case QAbstractSocket::OperationError:
        break;
    case QAbstractSocket::SslInternalError:
        break;
    case QAbstractSocket::SslInvalidUserDataError:
        break;
    case QAbstractSocket::TemporaryError:
        break;
    case QAbstractSocket::UnknownSocketError:
        break;
    default:
        break;
    }
    qDebug() << QString("socket(%1,%2,%3) error:"+client->errorString())
                .arg(tranid).arg(netarg.ipStr).arg(netarg.port);
    linking = !disconnectMyHost();
}

void MySocketTran::writeData()
{
    if(NULL==client){
        return;
    }
    if(QAbstractSocket::ConnectedState!=client->state())
    {
        return;
    }
    WDS it;
    if(this->getFirstWDS(it))
    {
//        qDebug() << QString("will send info(%1,%2,%3) to pcsSrv")
//                    .arg(it.pID).arg((int)it.pType).arg(it.val,0,'f',2);
        try{
            unsigned char buf[512] = { 0 };
            int len = this->getBuffer(buf);
            if(len>0){
                switch (netarg.type) {
                case 1:
                {
                    unsigned char* _buf = new unsigned char[2 * len + 1];
                    memset(_buf, 0, 2 * len + 1);
                    int nLen = PFunc::code(buf, len, _buf);//序列化处理
                    client->write((const char*)_buf, nLen);
                    bool ret = client->waitForBytesWritten();
//                    bool ret = client->flush();
                    if (!ret) {
                        qWarning() << QString("send point data: %1.")
                                    .arg(nLen);
                    }
                    else{
                        ativity = true;
//                        for (int j = 0; j < nLen; j++) {
//                            printf("%02X", _buf[j]);
//                        }
//                        printf("\n");
                    }
                    delete[] _buf;
                    _buf = NULL;
                }
                break;
                case 2:
                {
                    client->write((const char*)buf, len);
//                    bool ret = client->flush();
                    bool ret = client->waitForBytesWritten();
                    if (!ret) {
                        qWarning() << QString("send point data: %1.")
                                    .arg(len);
                    }else{
                        ativity = true;
//                        for (int j = 0; j < len; j++) {
//                            printf("%02X", buf[j]);
//                        }
//                        printf("\n");
                    }
                }
                break;
                default:
                    break;
                }
            }else {
                qWarning() << QString("getBuffer retLen(%1)").arg(len);
            }
        }
        catch(...){
            qDebug() << QString("data tran is error.");
        }
    }
}

int MySocketTran::AddFrame(const unsigned char *buf, int len)
{
    switch (netarg.ptype)
    {
    case 1:
        return AddFrameTran(buf,len);
    case 2:
        return AddFrameDef(buf,len);
    default:
        return 0;
    }
}

int MySocketTran::AddFrameDef(const unsigned char *pBuf, int len)
{
    qInfo() << QString("AddFrameDef buf(%1,%2)").arg((char*)pBuf).arg(len);
    try {
        unsigned char type = pBuf[0];
        int idx = 1;
        switch (type)
        {
        case 0XF5:
        {
            int rlen = pBuf[idx++] + (pBuf[idx++] << 8);
            int pID = 0;
            pID += pBuf[idx++];
            pID += (pBuf[idx++] << 8);
            pID += (pBuf[idx++] << 16);
            pID += (pBuf[idx++] << 24);
            PType pType = (PType)pBuf[idx++];
            float Value = float(0.0);
            unsigned char* fval = (unsigned char*)&Value;
            fval[3] = pBuf[idx++];
            fval[2] = pBuf[idx++];
            fval[1] = pBuf[idx++];
            fval[0] = pBuf[idx++];
            fval = NULL;
            //task id
            int taskID = 0;
            taskID += pBuf[idx++];
            taskID += (pBuf[idx++] << 8);
            taskID += (pBuf[idx++] << 16);
            taskID += (pBuf[idx++] << 24);
            //end
            unsigned char endflag = pBuf[idx++];
            if (OnYX == pType) {
                Value = Value > 0 ? float(1.0) : float(0.0);
            }
            if(0XFF!=endflag&&(rlen+1)!=idx)
            {
                qWarning() << QString("AddFrameDef(0XF5) end flag is error,rlen(%1)").arg(rlen);
            }else{
                m_WDData->addRDS(RDS(OnGet, pID, pType, Value,taskID));
            }
            //printf_s("read data 0XF5:pID[%d],pType[%d],val[%.3f],endflag[%02X]\n"
            //	,  pID, pType, Value, endflag);
            //test
            //m_WDData->addWDS(WDS(pType, pID,1.0));

        }
        break;
        case 0XF6:
        {
            int rlen = pBuf[idx++] + (pBuf[idx++] << 8);
            int pID = 0;
            pID += pBuf[idx++];
            pID += (pBuf[idx++] << 8);
            pID += (pBuf[idx++] << 16);
            pID += (pBuf[idx++] << 24);
            PType pType = (PType)pBuf[idx++];
            float Value = float(0.0);
            unsigned char* fval = (unsigned char*)&Value;
            fval[3] = pBuf[idx++];
            fval[2] = pBuf[idx++];
            fval[1] = pBuf[idx++];
            fval[0] = pBuf[idx++];
            fval = NULL;
            //task id
            int taskID = 0;
            taskID += pBuf[idx++];
            taskID += (pBuf[idx++] << 8);
            taskID += (pBuf[idx++] << 16);
            taskID += (pBuf[idx++] << 24);
            //end
            unsigned char endflag = pBuf[idx++];
            if (OnYX == pType) {
                Value = Value > 0 ? float(1.0) : float(0.0);
            }
            if(0XFF!=endflag&&(rlen+1)!=idx)
            {
                qWarning() << QString("AddFrameDef(0XF6) end flag is error,rlen(%1)").arg(rlen);
            }else{
                m_WDData->addRDS(RDS(OnSet, pID, pType, Value,taskID));
            }
            //printf_s("read data 0XF6:pID[%d],pType[%d],val[%.3f],endflag[%02X]\n"
            //	, pID, pType, Value, endflag);
        }
        break;
        default:
        {
            qWarning() << QString("Frame(%1,%2)  error.")
                        .arg((char*)pBuf).arg(len);
        }
        break;
        }
    }
    catch (...)
    {
        qDebug() << QString("AddFrameDef(%1,%2) func error.")
                    .arg((char*)pBuf).arg(len);
        return -1;
    }
    return 1;
}

int MySocketTran::AddFrameTran(const unsigned char *buf, int len)
{
    qInfo() << QString("AddFrameTran buf(%1,%2)").arg((char*)buf).arg(len);
    if (1 == netarg.ptype&&NULL != myScript)
    {
        try {
            char _buf[256] = { 0 };
            //转16进制字符串
            int rlen = PFunc::bytes2string(buf, _buf, len);
            QString atres = QString::fromLocal8Bit((char*)_buf,rlen);
            QScriptValueList args;
            args<<atres << "";
            QString retVal = myScript->myFunc("getReciveIDVAL",args).toString();
            if(!retVal.isEmpty()){
                QStringList retList = retVal.split(';', QString::SkipEmptyParts);
                for(int i=0; i<retList.size(); i++){
                    QStringList pList = retList.at(i).split(',', QString::SkipEmptyParts);
                    if(5==pList.size()){
                        int pID = pList.at(0).toInt();
                        float Value = pList.at(1).toFloat();
                        PType pType = (PType)pList.at(2).toInt();
                        ExeType exeType = (ExeType)pList.at(3).toInt();
                        unsigned long taskID = pList.at(4).toLong();
                        m_WDData->addRDS(RDS(exeType, pID, pType, Value,taskID));
                    }
                }
            }
            return 1;
        }
        catch (...) {
            qDebug() << QString("AddFrameTran(%1,%2) func error.")
                        .arg((char*)buf).arg(len);
            return -1;
        }
    }
    return -1;
}

int MySocketTran::getBuffer(unsigned char * _buf)
{
    switch (netarg.ptype)
    {
    case 1:
        return getBufferTran(_buf);
    case 2:
        return getBufferDef(_buf);
    default:
        return 0;
    }
};

int MySocketTran::getBufferDef(unsigned char * buf)
{
//    qInfo() << QString("getBufferDef now");
    WDS it;
    if(this->getFirstWDS(it)){
        try{
            int idx = 0;
            //unsigned char buf[512] = {0};

            buf[idx++] = 0xF7;		//0-STX
            //信息长度
            buf[idx++] = 0xf0;		//1-Len_L
            buf[idx++] = 0xf0;		//2-Len_H
            //时间
            //buf[idx++] = (unsigned char)(it.evtTimeS & 0xff);
            //buf[idx++] = (unsigned char)((it.evtTimeS >> 8) &0xff);
            //buf[idx++] = (unsigned char)((it.evtTimeS >> 16) &0xff);
            //buf[idx++] = (unsigned char)((it.evtTimeS >> 24) &0xff);
            //buf[idx++] = (unsigned char)(it.evtTimeMS & 0xFF);		//时间毫秒值

            //pid
            buf[idx++] = (unsigned char)(it.pID & 0xff);
            buf[idx++] = (unsigned char)((it.pID >> 8) & 0xff);
            buf[idx++] = (unsigned char)((it.pID >> 16) & 0xff);
            buf[idx++] = (unsigned char)((it.pID >> 24) & 0xff);
            //
            buf[idx++] = (unsigned char)(it.pType & 0xff);
            //val
            unsigned char*vavch = (unsigned char*)&(it.val);
            buf[idx++] = vavch[3];
            buf[idx++] = vavch[2];
            buf[idx++] = vavch[1];
            buf[idx++] = vavch[0];
            vavch = NULL;
            //task id
            unsigned long taskID = it.upChangeF?it.taskID:0;
            buf[idx++] = (unsigned char)(taskID & 0xff);
            buf[idx++] = (unsigned char)((taskID >> 8) & 0xff);
            buf[idx++] = (unsigned char)((taskID >> 16) & 0xff);
            buf[idx++] = (unsigned char)((taskID >> 24) & 0xff);
            //comment len
            buf[1] = static_cast<unsigned char>(idx & 0xff);
            buf[2] = static_cast<unsigned char>((idx >> 8) & 0xff);

            unsigned char m_end = 0xFF;
            memcpy(buf+idx,&m_end,1);
            idx++;

            if(!this->removeFirstWDS())
            {
                qDebug() << QString("removeFirstWD failed!");
            }
//            printf_s("write data 0xF7:pID[%d],pType[%d],val[%.3f]\n"
//                , it.pID, it.pType, it.val);
            return idx;
        }catch(...){
            //printf_s("write item info to socket failed! have error. \r\n");
            qDebug() << QString("write item info to socket failed! have error.");
            return -1;
        }
    }else{
        return -1;
    }
};

int MySocketTran::getBufferTran(unsigned char * buf)
{
//    qInfo() << QString("getBufferTran now");
    WDS it;
    if (this->getFirstWDS(it))
    {
        try {
            //you code for item data to cmd buf
            QString _cmd = "";
            int checktype = 0;
//            QString _errormsg = "";
            memset(buf, 0, 256);
            int len = 0;
            //生成16进制字符串命令
            QScriptValueList args;
            args<<(int)OnTran << it.pType << it.pID << it.val << (int)it.taskID;
            QString retVal = myScript->myFunc("getDownControl",args).toString();
            if(!retVal.isEmpty()){
                QStringList retList = retVal.split(',', QString::SkipEmptyParts);
                if(2!=retList.size()){
                    qWarning() << QString("js to code inf(%1,%2,%3,%4) is error")
                                .arg((int)OnTran).arg(it.pType).arg(it.pID).arg(it.val,0,'f',2);
                    return -1;
                }
                _cmd = retList.at(0);
                checktype = retList.at(1).toInt();
                switch (checktype)
                {
                case 1:
                {
                    //转ACSII字符串
                    len = PFunc::string2bytes(_cmd.toStdString().c_str(), buf, _cmd.length());
                    unsigned int crc = PFunc::crc16(buf, len);
                    sprintf((char*)buf, "%s%02X%02X", _cmd.toStdString().c_str(), (crc & 0XFF), ((crc >> 8) & 0XFF));
                    _cmd = QString::fromLocal8Bit((char*)buf);
                    len = _cmd.length();
                }
                break;
                case 2:
                {
                    len = PFunc::string2bytes(_cmd.toStdString().c_str(), buf, _cmd.length());
                    //for (int j = 0; j < len; j++) {
                    //	printf("%02X", buf[j]);
                    //}
                    //printf("\n");
                    _cmd = QString::fromLocal8Bit((char*)buf, len);
                }
                break;
                default:
                    break;
                }
                if (!this->removeFirstWDS())
                {
                    //printf_s("removeFirstWDS failed!\r\n");
                    qWarning() << QString("removeFirstRDTran failed!");
                }
                return len;
            }
            else
            {
                qWarning() << QString("getBufferTran Error:exeType(%1),pID(%2),pType(%3),val(%4)")
                            .arg((int)OnTran).arg(it.pID).arg((int)it.pType).arg(it.val,0,'f',2);
            }
        }
        catch (...)
        {
            qDebug() << QString("getBufferTran for data to cmd error.");
        }
    }
    return -1;
};

//
int MySocketTran::getRDSize()
{
    int size = 0;
    m_MutexRD.lock();
    size = ReadData.size();
    m_MutexRD.unlock();
    return size;
}

bool MySocketTran::isEmptyRD()
{
    bool ret = false;
    m_MutexRD.lock();
    ret = ReadData.empty();
    m_MutexRD.unlock();
    return ret;
}

bool MySocketTran::getFirstRD(RD &it) {
    if (!isEmptyRD()) {
        m_MutexRD.lock();
        it = ReadData.front();
        m_MutexRD.unlock();
        return true;
    }
    else {
        return false;
    }
}

bool MySocketTran::removeFirstRD() {
    if (!isEmptyRD()) {
        m_MutexRD.lock();
        ReadData.pop_front();
        m_MutexRD.unlock();
        return true;
    }
    else {
        return false;
    }
}

void MySocketTran::addRD(RD it) {
    if (getRDSize() > QSize) {
        _ReadData_over++;
        if (_ReadData_over >= 10) {//每溢出10次，报告一次
            _ReadData_over = 0;
            qWarning() << QString("the size of ReadData vector is up to limmit size: %1 .")
                        .arg(QSize);
        }
        removeFirstRD();
    }
    m_MutexRD.lock();
    ReadData.append(it);
    m_MutexRD.unlock();
}
//
int MySocketTran::getWDSSize()
{
    int size = 0;
    m_MutexWDS.lock();
    size = WriteDataS.size();
    m_MutexWDS.unlock();
    return size;
}

bool MySocketTran::isEmptyWDS()
{
    bool ret = false;
    m_MutexWDS.lock();
    ret = WriteDataS.empty();
    m_MutexWDS.unlock();
    return ret;
}

bool MySocketTran::getFirstWDS(WDS &it) {
    if (!isEmptyWDS()) {
        m_MutexWDS.lock();
        it = WriteDataS.front();
        m_MutexWDS.unlock();
        OnDoIt_WDS = true;
        return true;
    }
    else {
        return false;
    }
}

bool MySocketTran::removeFirstWDS() {
    if (!isEmptyWDS()) {
        m_MutexWDS.lock();
        WriteDataS.pop_front();
        m_MutexWDS.unlock();
        return true;
    }
    else {
        return false;
    }
}

void MySocketTran::addWDS(WDS it) {
    if (getWDSSize() > QSize) {
        _WriteData_overS++;
        if (_WriteData_overS >= 10) {//每溢出10次，报告一次
            //正在处理中溢出才报告,无处理不做报告
            if (OnDoIt_WDS) {
                OnDoIt_WDS = false;
                qWarning()<< QString("the size of MySocketTran::WriteDataS vector is up to limmit size: %1 .")
                           .arg(QSize);
            }
            _WriteData_overS = 0;
        }
        removeFirstWDS();
    }
    m_MutexWDS.lock();
    WriteDataS.append(it);
    m_MutexWDS.unlock();
}


qint64 MySocketTran::getSec()
{
    return QDateTime::currentSecsSinceEpoch();
}

qint64 MySocketTran::getMSec()
{
    return QDateTime::currentMSecsSinceEpoch();
}
