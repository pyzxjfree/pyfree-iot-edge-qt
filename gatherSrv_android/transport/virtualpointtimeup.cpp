#include "virtualpointtimeup.h"

#include <QDebug>

#include "data/mycachedata.h"

VirtualPointTimeUp::VirtualPointTimeUp(QObject *parent)
    : QThread(parent)
    , running(true)
{
    ptr_MyCacheData = MyCacheData::getInstance();
    upPInfoTime = ptr_MyCacheData->getSec() + 60;
}

VirtualPointTimeUp::~VirtualPointTimeUp()
{
    running=false;
    try{
        this->terminate();
        this->wait();
        qInfo() << "VirtualPointTimeUp::destroy\n";
    }catch(...){
        qDebug() << "VirtualPointTimeUp::destroy fail\n";
    }
}

void VirtualPointTimeUp::run()
{
    while (running) {
        this->timeUpVirtualPInfo();
        this->msleep(100);
    }
    exec();
}

void VirtualPointTimeUp::timeUpVirtualPInfo()
{
    if(upPInfoTime<ptr_MyCacheData->getSec())
    {
        upPInfoTime = ptr_MyCacheData->getSec()+10;
        ptr_MyCacheData->TimeUpVirtualPInfo();
    }
}
