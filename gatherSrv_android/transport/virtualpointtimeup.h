#ifndef VIRTUALPOINTTIMEUP_H
#define VIRTUALPOINTTIMEUP_H
/*create virtual point send info to localservice,comXF,McsSrv by interval time*/
#include <QThread>

class MyCacheData;

class VirtualPointTimeUp : public QThread
{
    Q_OBJECT
public:
    VirtualPointTimeUp(QObject *parent = Q_NULLPTR);
    virtual ~VirtualPointTimeUp();
protected:
    virtual void run();
private:
    void timeUpVirtualPInfo();
private:
    bool running;
    unsigned int upPInfoTime;
    MyCacheData *ptr_MyCacheData;
};
#endif // VIRTUALPOINTTIMEUP_H
