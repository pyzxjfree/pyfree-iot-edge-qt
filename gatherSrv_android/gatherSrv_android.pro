QT += core
QT += serialport
QT += xml
QT += script
QT += network
QT += gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
greaterThan(QT_MAJOR_VERSION, 4): QT += concurrent

TEMPLATE = app

SOURCES += main.cpp \
    src/gathermgr.cpp \
    serialport/serialportthread.cpp \
    serialport/serialport.cpp \
    netport/netport.cpp \
    config/configread.cpp \
    data/mycachedata.cpp \
    data/myfunc.cpp \
    data/comcachedata.cpp \
    data/hashmap.cpp \
    data/receivedata.cpp \
    script/MyScript.cpp \
    transport/mysockettran.cpp \
    transport/mysocketsrv.cpp \
    transport/comxfsys.cpp \
    routing/planthread.cpp \
    log/mylog.cpp \
    log/logmonitor.cpp \
    transport/virtualpointtimeup.cpp


# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

HEADERS += \
    src/gathermgr.h \
    serialport/serialportthread.h \
    serialport/serialport.h \
    netport/netport.h \
    config/confdef.h \
    config/configread.h \
    data/mycachedata.h \
    data/myfunc.h \
    data/datadef.h \
    data/comcachedata.h \
    data/hashmap.h \
    data/receivedata.h \
    script/MyScript.h \
    transport/mysockettran.h \
    transport/mysocketsrv.h \
    transport/comxfsys.h \
    routing/planthread.h \
    log/mylog.h \
    log/logmonitor.h \
    transport/virtualpointtimeup.h \
    src/version.h \

#transport/mysockettranthread.h

#翻译
TRANSLATIONS += languages/gather_srv_control_cn.ts
#资源
RESOURCES += gatherSrv_android.qrc

target.path = ..
INSTALLS += target

DESTDIR = bin

CONFIG(debug, debug|release) {
    win32{
        TARGET          = gatherSrv_wind
        OBJECTS_DIR     = debug_win/obj
        MOC_DIR         = debug_win/moc
    }
    unix{
        TARGET          = gatherSrv_linuxd
        OBJECTS_DIR     = debug_linux/obj
        MOC_DIR         = debug_linux/moc
    }
    android-g++ {
        TARGET          = gatherSrv_android
        OBJECTS_DIR     = debug_android/obj
        MOC_DIR         = debug_android/moc
    }
    CONFIG          += console
    DEFINES         += _DEBUG
}else{
    win32{
        TARGET          = gatherSrv_win
        OBJECTS_DIR     = release_win/obj
        MOC_DIR         = release_win/moc
    }
    unix{
        TARGET          = gatherSrv_linux
        OBJECTS_DIR     = release_linux/obj
        MOC_DIR         = release_linux/moc
    }
    android-g++ {
        TARGET          = gatherSrv_android
        OBJECTS_DIR     = release_android/obj
        MOC_DIR         = release_android/moc
    }
    #DEFINES         += NODEBUG #install service printf isn't effective
    DEFINES         += NODEBUG
    CONFIG          += console
}
android-g++ {
    #TARGET = gatherSrv_android
    QT += androidextras
    LIBS += -lgnustl_shared
    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

    HEADERS += \
    javaSrc/qDebug2Logcat.h \
    javaSrc/simpleCustomEvent.h \
    javaSrc/javanative.h

    SOURCES += \
    javaSrc/javanative.cpp \
    javaSrc/qDebug2Logcat.cpp \
    javaSrc/simpleCustomEvent.cpp \

    DISTFILES += \
    android/AndroidManifest.xml \
    android/libs/android-support-v4.jar \
    android/libs/pl2303multilib.jar \
    android/res/xml/device_filter.xml \
    android/src/an/qt5/javagather/PL2303HXDNative.java \
    android/src/an/qt5/javagather/PL2303HXDSerialPort.java \
    android/src/an/qt5/javagather/GatherBootUpReceiver.java \
    #android/src/android/hardware/usb/IUsbManager.java \
    #android/src/android/os/ServiceManager.java \
    #android/src/an/qt5/javagather/UsbDeviceDescriptor.java \
    #android/src/an/qt5/javagather/SystemManager.java \
    #android/src/an/qt5/javagather/StubService.java \
    android/assets/modbus.js \
    android/assets/tcp.js \
    android/assets/tcpPlay.js \
    android/assets/gather.xml \
    android/assets/gmap.xml

}

win32{
    CONFIG += c++11
    DEFINES -= UNICODE
    SOURCES += svc_common/WinSVC.cpp
    system(SubWCRev ./ src/version.h__ src/version.h){
        message( "get new version file!" )
    }
    #for_android need do it for log or about_sofeware
    RC_FILE += app.rc
}

DISTFILES += \
    readme.txt

