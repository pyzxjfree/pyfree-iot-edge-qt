#ifndef MYSCRIPT_H
#define MYSCRIPT_H

#include <QTextStream>
#include <QScriptEngine>
#include <QScriptValue>
#include <QScriptValueList>

class MyScript
{
public:
	MyScript(QString _file);
	~MyScript();

	QScriptValue myFunc(QString _funcName,QScriptValueList args
		,QString rarg=QString(),QScriptValue *rval= (new QScriptValue()));

    bool openScript();
private:
    QString scriptFile;
    bool activity;
	QScriptEngine engine;
};

#endif
