#ifndef GATHERMGR_H
#define GATHERMGR_H

#include <QObject>
#include <QWidget>
#include <QMap>
#include <QThread>
#include <QSystemTrayIcon>

QT_BEGIN_NAMESPACE
class MyCacheData;
class SerialPort;
class NetPort;
class MySocketTran;
class QPlainTextEdit;
class ReceiveData;
class VirtualPointTimeUp;
class ComXFSys;
class PlanThread;
class MySocketSrv;
class QAction;
class QMenu;
QT_END_NAMESPACE

class GatherMgrThread : public QThread
{
    Q_OBJECT
public:
    GatherMgrThread(MyCacheData* _mycdata
                    ,QMap<int,SerialPort*> _serialps
                    ,QMap<int,NetPort*> _netGathers
                    ,QMap<int,MySocketTran*> _clients
                    ,QObject *parent = Q_NULLPTR);
    ~GatherMgrThread();
protected:
    virtual void run();
private:
    MyCacheData *mycdata;
    QMap<int,SerialPort*> serialps;
    QMap<int,NetPort*> netGathers;
    QMap<int,MySocketTran*> clients;
    ReceiveData *ptr_ReceiveData;
    bool running;
};

class GatherMgr : public QWidget
{
    Q_OBJECT
public:
    GatherMgr(QWidget *parent = Q_NULLPTR);
    ~GatherMgr();
protected:
    bool event(QEvent *);

    void changeEvent ( QEvent * event );
    void hideEvent(QHideEvent *event);
//    void closeEvent(QCloseEvent *event);
private:
    void initGatherChannel();

    void initTray();
//    bool closeEventEnsure();
	void showMini();
	void moveToFront();
signals:
    void notify_MsgAndroid(QString msg,int pindex);
public slots:
    void displayMsg(QString msg);
//    void serialPortMaybeNoPermissionSlot();
private slots:
    void trayiconActivated(QSystemTrayIcon::ActivationReason reason);
    void myquit();
private:
    MyCacheData *mycdata;
    QMap<int,SerialPort*> serialps;
    QMap<int,NetPort*> netGathers;
    QMap<int,MySocketTran*> clients;
    QPlainTextEdit *td_view;
    VirtualPointTimeUp *ptr_VirtualPointTimeUp;
    ComXFSys *ptr_ComXFSys;
    PlanThread *ptr_PlanThread;
    MySocketSrv *ptr_MySocketSrv;
    GatherMgrThread *ptr_GatherMgrThread;

    QSystemTrayIcon *trayIcon;
    QAction *minimizeAction;
    QAction *restoreAction;
//    QAction *helpAction;
    QAction *quitAction;
    QMenu   *trayIconMenu;
};

#endif // GATHERMGR_H
