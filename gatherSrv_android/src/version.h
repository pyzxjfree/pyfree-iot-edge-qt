////版本源文件
#pragma once

#define VERSION_BUILD_NUMBER 		0
#define STR_BUILD_NUMBER 		"0"
#define STR_VERSION_BUILD 		"(B0 2018/07/18 14:42:13)"
#define STR_VERSION_DATE 		"1970/01/01 08:00:00"
#define STR_VERSION_URL 		"http://mips.syeamt.com/repos/SyeMonitor/gatherSrv_android/gatherSrv_android"
#define STR_VERSION_NOW 		"2018/07/18 14:42:13"
#define NSTR_VERSION_NOW 		2018/07/18 14:42:13

#define STR_VERSION_VERSION		"V1.2.1.1"
#define STR_VERSION_COMPANY		"珠海兴业新材料科技有限公司"
#define STR_VERSION_FILEDESC		"投影中控系統"
#define STR_VERSION_FILEVER		STR_VERSION_VERSION
#define STR_VERSION_LEGAL		"Copyright(C) 2017 All rights reserved"
#define STR_VERSION_PRODUCTNAME		"兴业新材料_中控软件"
#define STR_VERSION_TELE		"400-602-39188"
#define STR_VERSION_FAX			"400-602-39188"	
#define STR_VERSION_WEB			"http://www.syeamt.com"
#define STR_VERSION_POSTCODE		"519080"
#define STR_VERSION_ADDRESS		"珠海高新区创新海岸金珠路九号"
