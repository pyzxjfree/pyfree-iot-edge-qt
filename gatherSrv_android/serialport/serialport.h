#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <QObject>
#include <QMutex>

QT_BEGIN_NAMESPACE
class MyCacheData;
class SerialPortThread;
class MsgAnalysisThread;
class MyScript;
class ReceiveData;
QT_END_NAMESPACE

#include <QtSerialPort/QSerialPort>
#include "data/datadef.h"
#include "config/confdef.h"
/*
"9600", QSerialPort::Baud9600
"19200", QSerialPort::Baud19200
"38400", QSerialPort::Baud38400
"115200", QSerialPort::Baud115200

"5", QSerialPort::Data5
"6", QSerialPort::Data6
"7", QSerialPort::Data7
"8", QSerialPort::Data8

QObject::tr("None"), QSerialPort::NoParity
QObject::tr("Even"), QSerialPort::EvenParity
QObject::tr("Odd"), QSerialPort::OddParity
QObject::tr("Mark"), QSerialPort::MarkParity
QObject::tr("Space"), QSerialPort::SpaceParity

"1", QSerialPort::OneStop
"2", QSerialPort::TwoStop

QObject::tr("None"), QSerialPort::NoFlowControl
QObject::tr("RTS/CTS"), QSerialPort::HardwareControl
QObject::tr("XON/XOFF"), QSerialPort::SoftwareControl
*/
struct QSerialSettings {
    QSerialSettings()
        : name("COM6")
        , baudRate(19200)
        , stringBaudRate("19200")
        , dataBits(QSerialPort::Data8)
        , stringDataBits("8")
        , parity(QSerialPort::NoParity)
        , stringParity(QObject::tr("None"))
        , stopBits(QSerialPort::OneStop)
        , stringStopBits("1")
        , flowControl(QSerialPort::NoFlowControl)
        , stringFlowControl(QObject::tr("None"))
    {

    }

    QString name;
    qint32 baudRate;
    QString stringBaudRate;
    QSerialPort::DataBits dataBits;
    QString stringDataBits;
    QSerialPort::Parity parity;
    QString stringParity;
    QSerialPort::StopBits stopBits;
    QString stringStopBits;
    QSerialPort::FlowControl flowControl;
    QString stringFlowControl;
    unsigned int readCount;
    unsigned int readSleep;
    unsigned int timePush;
};

struct PortState
{
    PortState() : m_bExit(true)
        , m_wf(false)
        , m_rf(false)
        , m_tc(true)
    {

    };
    bool m_bExit;//端口是否关闭
    bool m_wf;//on writing flag
    bool m_rf;//on reading flag
    bool m_tc;//total call flag,是否允许总召
    //通信状态上送时间
    unsigned int PortStateTime;
};

class SerialPort : public QObject
{
    Q_OBJECT
public:
    SerialPort(int _gatherID,QString _name,SerialPortArg _spArg,ProtocolDef _pdArg,QList<PInfo> _pinfos
               ,QObject *parent = Q_NULLPTR);
    ~SerialPort();

    void downControl(int _exetype, int _type, int _pid, float _val);
private:
    void controlGState(int _exetype, int _type, int _pid, float _val);
    //地铁展厅继电器及传感器使用
    int getAllYXVal(int _start, int _end);
    void serialConf(SerialPortArg _spArg);
    void allCallCmdInit(CMDLib &_cmds);
    void pinfoTag();
    void setVal(int index, float val);
    void setWDSVal(int pid, PType type, float value); 
signals:
    void logNotify(QString _log);
//    void serialPortMaybeNoPermission();
public slots:
    void sendMsg(QString cmd, bool u16f=true);
    void getMsgAndroid(QString msg, int pindex=0);
    void uncodeMsg(QString hexMsg,QString orederMsg);
    void TimeUp();
private slots:
    void readData();
#ifndef ANDROID
    void handleError(QSerialPort::SerialPortError error);
#endif
    void openSerialPort();
    void closeSerialPort();
private:
    PortState m_PortState;
    MyCacheData *mycachedata;
#ifndef ANDROID
    QSerialPort *serial;
#else
    bool serialState;
#endif
    QSerialSettings serialconfig;
    SerialPortThread *serialThread;
    MsgAnalysisThread *msganalysisThread;
    MyScript *myScr;
    QMutex myScr_Mutex;
    //
    int gatherID;
    ProtocolDef pdArg;
    QList<PInfo> pinfos;
    //
    ReceiveData *ptr_ReceiveData;
    QMutex m_Map_Mutex;
    //冗余信息点存储,方便快速查询
    QMap<int,PInfo> addr_to_ids;
    QMap<int,PInfo> id_to_addrs;

    QString oldDownCmd;
    //
    static float FLIMIT;
};
#endif // SERIALPORT_H
