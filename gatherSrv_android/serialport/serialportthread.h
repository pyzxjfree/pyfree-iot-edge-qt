#ifndef SERIALPORTTHREAD_H
#define SERIALPORTTHREAD_H

#include <QThread>
#include <QQueue>
#include <QMutex>

#include "data/datadef.h"

QT_BEGIN_NAMESPACE
class SerialPort;
QT_END_NAMESPACE

struct DownCmd{
    DownCmd(QString _cmd,bool _uf)
        : cmd(_cmd),u16f(_uf)
    {

    };

    QString cmd;
    bool  u16f;
};

struct UpCmd{
    UpCmd():retCmd(""),downCmd("")
    {

    };

    UpCmd(QString _ret,QString _send)
        : retCmd(_ret),downCmd(_send)
    {

    };

    QString retCmd;
    QString downCmd;
};

class SerialPortThread : public QThread
{
    Q_OBJECT
public:
    SerialPortThread(CMDLib _cmds,SerialPort *_SerialPort,QObject *parent = Q_NULLPTR);
    ~SerialPortThread();

    void AddMsg(QString cmd, bool u16f,bool allCallf=true);
    void setAllCall(bool openflag);
    bool getAllCall();
protected:
    virtual void run();
private:
    qint64 getMsec();
    void allCall();
    void doCmd();
signals:
//    void readData();
    void sendMsg(QString cmd, bool u16f);
    void TimeUp();
public slots:
//    void cacheForReady();
private slots:

private:
    QQueue<DownCmd> dcms;
    QMutex dcmds_mutex;
    SerialPort *ptr_SerialPort;
//    bool cacheForRead;
    CMDLib cmds;
    bool running;
    int limitSize;
    qint64 allCallTime;
    bool allCallFlag;
};

class MsgAnalysisThread : public QThread
{
    Q_OBJECT
public:
    MsgAnalysisThread(SerialPort *_SerialPort,QObject *parent = Q_NULLPTR);
    ~MsgAnalysisThread();

    void AddMsg(QString _ret, QString _send);
protected:
    virtual void run();
private:
//    qint64 getMsec();
    void doCmd();
signals:
//    void sendMsg(QString cmd, bool u16f);
public slots:

private slots:

private:
    QQueue<UpCmd> upcms;
    QMutex upcms_mutex;
    SerialPort *ptr_SerialPort;
    bool running;
    int limitSize;
};

#endif // SERIALPORTTHREAD_H
