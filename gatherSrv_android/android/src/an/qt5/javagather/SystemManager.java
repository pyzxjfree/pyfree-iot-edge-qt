package an.qt5.javagather;

import java.io.DataOutputStream;
import android.app.Activity;
import java.io.File;

public class SystemManager extends Activity {
    /**
    * 判断当前手机是否有ROOT权限
    * @return
    */
   public static boolean isRoot(){
       boolean bool = false;

       try{
           if ((!new File("/system/bin/su").exists()) && (!new File("/system/xbin/su").exists())){
               bool = false;
           } else {
               bool = true;
           }
           PL2303HXDNative.OnNotify_LOG("root bool = " + bool);
       } catch (Exception e) {

       }
       return bool;
   }
    /**
     * 应用程序运行命令获取 Root权限，设备必须已破解(获得ROOT权限)
     * @param command 命令：String apkRoot="chmod 777 "+getPackageCodePath(); RootCommand(apkRoot);
     * @return 应用程序是/否获取Root权限
     */
    public static boolean RootCommand(String command)
    {
        Process process = null;
        DataOutputStream os = null;
        try
        {
            process = Runtime.getRuntime().exec("su");
            os = new DataOutputStream(process.getOutputStream());
            os.writeBytes(command + "\n");
            os.writeBytes("exit\n");
            os.flush();
            process.waitFor();
        } catch (Exception e)
        {
            PL2303HXDNative.OnNotify_LOG("*** DEBUG ***" + "ROOT REE" + e.toString());
            return false;
        } finally
        {
            try
            {
                if (os != null)
                {
                    os.close();
                }
                process.destroy();
            } catch (Exception e)
            {
            }
        }
        PL2303HXDNative.OnNotify_LOG("*** DEBUG ***" + "Root SUC ");
        return true;
    }
}
