package an.qt5.javagather;

//二选一，确定USB设备
public class UsbDeviceDescriptor {

    public String packageName;

    //第一组，确定USB设备
    public int deviceClass;
    public int deviceSubclass;
    //第二级，确定USB设备
    public int vendorId;
    public int productId;

    @Override
    public String toString() {
        return "packageName:" + packageName
                + ", deviceClass:" + deviceClass
                + ", deviceSubclass: " + deviceSubclass
                + ", vendorId: " + vendorId
                + ", productId: " + productId;
    }

}
