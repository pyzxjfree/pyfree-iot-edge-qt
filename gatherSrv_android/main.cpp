#include <QApplication>
#include <QTranslator>
#include <QTextCodec>

#ifdef ANDROID
#include "javaSrc/qDebug2Logcat.h"
#include "javaSrc/simpleCustomEvent.h"
#include "javaSrc/javanative.h"
#endif
#include "src/gathermgr.h"
#include "log/mylog.h"
#include "log/logmonitor.h"


namespace GVAL_Gather {
    QObject *g_listener = 0;
}
namespace AppLog {
    QString dateStr="";
    QString timeStr="";
}

#ifdef WIN32
//server conf
char SVCNAME[128] = "SYE_GATHER_C_SRV_APP";
char SVCDESC[256] =
"\r\n Zhuhai Singyes application Mstar Technology Ltd \r\n "
"http://www.syeamt.com \r\n "
"Telephone: 0756-6916731\r\n "
"Singyes pojection system service \r\n "
"Service installation success";

int MyMain(int argc, char* argv[])
#else
int main(int argc, char* argv[])
#endif
{
    QApplication app(argc, argv);
#ifndef _DEBUG
    //log
    AppLog::initLog();
    //加载日志模块
    qInstallMessageHandler(AppLog::logMsgHandler);
    //
    LogMonitor mylog_;
    mylog_.start();
#endif
    //languages translation, main event is translate Marco tr("")
    QTranslator translator;
    //装载翻译文件
    //lupdate *.pro导出源文代码
    translator.load(":/languages/gather_srv_control_cn.qm");
    //app.setFont(QFont("wenquanyi")); //set font stype lib
    app.installTranslator(&translator);
    //设置本地语言
    QTextCodec::setCodecForLocale(QTextCodec::codecForLocale());
//    //设置UTF-8
//    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
//    QTextCodec::setCodecForLocale(codec);
#ifdef ANDROID
    // android even register
    SimpleCustomEvent::eventType();
    registerNativeMethods();
#endif
    GatherMgr gatherMgr;
#ifdef ANDROID
    GVAL_Gather::g_listener = qobject_cast<QObject*>(&gatherMgr);
#endif
    gatherMgr.show();
    return app.exec();
}
