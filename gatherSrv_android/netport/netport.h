#ifndef NETPORT_H
#define NETPORT_H

#include <QObject>
#include <QThread>
#include <QAbstractSocket>
#include <QQueue>
#include <QMutex>

#include "config/confdef.h"
#include "data/datadef.h"

QT_BEGIN_NAMESPACE
class QTcpSocket;
class MyCacheData;
class ReceiveData;
class MyScript;
QT_END_NAMESPACE

struct NetState
{
    NetState(): m_wf(false), m_rf(false), m_tc(true)
        ,linking(false), ativity(false), heartTime(0)
        , newcmd("")
    {

    };
    //发送状态
    bool m_wf;
    //接收状态
    bool m_rf;
    //总召状态
    bool m_tc;
    //
    bool linking;
    unsigned int linkTime;
    bool ativity;
    unsigned int heartTime;
    //通信状态上送时间
    unsigned int netPortStateTime;
    QString newcmd;
};

struct NetCmd
{
    NetCmd() : len(0),u16f(false)
    {
        memset(Buf, 0, 512);
    };
    NetCmd(unsigned char *buf, int nlen)
        : len(nlen),u16f(false)
    {
        memset(Buf, 0, 512);
        memcpy(Buf, buf, nlen);
    };
    NetCmd(unsigned char *buf, int nlen, bool _f)
        : len(nlen),u16f(_f)
    {
        memset(Buf, 0, 512);
        memcpy(Buf, buf, nlen);
    };
    NetCmd& operator=(const NetCmd &rval)
    {
        if (this != &rval) {
            memset(Buf, 0, 512);
            if (rval.len <= 512) {
                memcpy(Buf, rval.Buf, rval.len);
                len = rval.len;
            }
            else {
                memcpy(Buf, rval.Buf, 512);
                len = 512;
            }
            u16f = rval.u16f;
        }
        return *this;
    };
    unsigned char Buf[512];
    int len;
    bool u16f;
};

class NetPort : public QThread
{
    Q_OBJECT
public:
    NetPort(int gid
            , NetArg _netarg
            , ProtocolDef _protodefs
            , QList<PInfo> _pinfos
            , int _controlsleep=50
            , QObject*parent = Q_NULLPTR);
    ~NetPort();

    void downControl(int _exetype, int _type, int _pid, float _val);
protected:
    virtual void run();
private:
    void allCall();
    void AddCmd(unsigned char*cmd, int len,bool uf,bool allCallf=true);
    bool getFirstCmd(NetCmd &ncmd);
    void doCmd();
    void allCallCmdInit(CMDLib &_cmds);
    bool initTcpSocket();
    bool destroyTcpSocket();
    bool connectMyHost();
    bool disconnectMyHost();
    void checkLink();
    void TimeUp();
    void checkReadyRead();
    //
    void readCache(const unsigned char *bufdata,int buflen);
    void msgResponse(unsigned char *buf, int len);
    void writeMsg(unsigned char*buf,int buflen,bool u16f);
    //
    void setValue(int _addr, float _val);
    void setPValue(int pid, PType type, float val);
    void writeData(unsigned char*buf,int buflen);
    int getAllYXVal(int _start, int _end);
    void controlGState(int _exetype, int _type, int _pid, float _val);
signals:
    void logNotify(QString _log);
private slots:
    void sockeError(QAbstractSocket::SocketError error);
    //
    void readData();
private:
    //采集编号
    int gatherID;
    //状态集
    NetState m_NetState;
    //网络链接配置
    NetArg netarg;
    //协议配置
    ProtocolDef protodefs;
    //
    MyCacheData *mycachedata;

    //
    ReceiveData *ptr_ReceiveData;
    QMutex m_PMap_Mutex;
    //冗余信息点存储,方便快速查询
    QMap<int,PInfo> addr_to_ids;
    QMap<int,PInfo> id_to_addrs;
    //
    MyScript *myScr;
    QMutex myScr_Mutex;
    //
    qint64 allCallTime;
    CMDLib cmds;
    bool running;
    //
    QTcpSocket *m_csocket;
    //
    QQueue<NetCmd> netcms;
    QMutex netcms_mutex;
    int limitSize;
    //
    static float FLIMIT;
};

#endif // NETPORT_H
