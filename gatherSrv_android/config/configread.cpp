#include "configread.h"

#include "data/myfunc.h"

#include <QFile>
#include <QDebug>

void XMLRead::readAppConf(MyConf &conf, QString filePath)
{
    QDomElement docElem;
    if(readXml(filePath,docElem)){
        readGatherDom(conf,docElem);
    }
}

void XMLRead::readGatherDom(MyConf &conf,QDomElement docElem)
{
    QDomNode n = docElem.firstChild();
    while(!n.isNull()) {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        if(!e.isNull()) {
//            qInfo() << e.tagName() <<"$"; // the node really is an element.
            if("TranIPJson"==e.tagName()){
                conf.ipCleintJson = e.text();
            }
            if("TranPortJson"==e.tagName()){
                conf.portClientJson = e.text().toInt();
            }
            if("PlayerPort"==e.tagName()){
                conf.listenPortPlayer = e.text().toInt();
            }
            if("LocalPort"==e.tagName()){
                conf.portLocal = e.text().toInt();
            }
            //space
            if("DiskSymbol"==e.tagName()){
                if(e.text().length()==1)
                    conf.diskSymbol = e.text().at(0).toLatin1();
            }
            if("FreeSizeLimit"==e.tagName()){
                conf.freeSizeLimit = e.text().toInt();
                if(conf.freeSizeLimit<1000)
                    conf.freeSizeLimit = 1000;
            }
            if("DataCurveDir"==e.tagName()){
                conf.gDataDir = e.text();
            }
            //path
            if("gatherPath"==e.tagName()){
                conf.gatherPath = e.text();
            }
            if("gmapPath"==e.tagName()){
                conf.gmapPath = e.text();
            }
            if("DevPath"==e.tagName()){
                conf.devPath = e.text();
            }
            if("PmapPath"==e.tagName()){
                conf.pmapPath = e.text();
            }
            if("PlanPath"==e.tagName()){
                conf.planPath = e.text();
            }
            //area
            if("AreaID"==e.tagName()){
                conf.areaId = e.text().toInt();
            }
            if("AreaType"==e.tagName()){
                conf.areaType = e.text().toInt();
            }
            if("AreaName"==e.tagName()){
                conf.areaName = e.text();
            }
            if("AreaDesc"==e.tagName()){
                conf.areaDesc = e.text();
            }
            //func
            if("YunFunc"==e.tagName()){
                conf.yunFunc = e.text().toInt();
            }
            if("McsFunc"==e.tagName()){
                conf.mcsFunc = e.text().toInt();
            }
            if("TimeUpVirtual"==e.tagName()){
                conf.timeUpVirtual = e.text().toInt();
            }
            if("ViewFunc"==e.tagName()){
                conf.viewFunc = e.text().toInt();
            }
        }
        n = n.nextSibling();
    }
}

void XMLRead::readGather(ComManagerDef &commdef,QString filePath)
{
    QDomElement docElem;
    if(readXml(filePath,docElem)){
        readGatherDom(commdef,docElem);
    }
}

void XMLRead::readGatherDom(ComManagerDef &commdef,QDomElement docElem)
{
    QDomNode n = docElem.firstChild();
    while(!n.isNull()) {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        if(!e.isNull()) {
//            qInfo() << e.tagName() <<"$"; // the node really is an element.
            if("transmit"==e.tagName()){
                QMap<QString,QString> atts;
                atts["id"]          = "";
                atts["trantype"]    = "";
                atts["dtype"]       = "";
                atts["prototype"]   = "";
                //
                readNodeAtts(atts,e);

                Transmit trans;
                trans.id = atts["id"].toInt();
                trans.trantype = atts["trantype"].toInt();
                trans.dtype = atts["dtype"].toInt();
                trans.protocoltype = atts["prototype"].toInt();
//                qInfo()<<"atts_size="<<atts.size();
//                qInfo()<<"add_tran_port="<<trans.id;
                commdef.trans.append(trans);
            }
            if("sptype"==e.tagName()){
                QMap<QString,QString> atts;
                atts["id"] = "";
                atts["name"] = "";
                atts["BaudRate"] = "";
                atts["DataBit"] = "";
                atts["Parity"] = "";
                atts["StopBit"] = "";
                atts["ReadCount"] = "";
                atts["ReadSleep"] = "";
                atts["TimePush"] = "";
                //
                readNodeAtts(atts,e);

                SerialPortArg sparg;
                sparg.id = atts["id"].toInt();
                sparg.name = atts["name"];
                sparg.BaudRate = atts["BaudRate"].toInt();
                sparg.DataBit = atts["DataBit"].toInt();
                sparg.Parity = atts["Parity"];
                sparg.StopBit = atts["StopBit"].toInt();
                sparg.readCount = 1;
                if(!atts["ReadCount"].isEmpty()){
                    sparg.readCount = atts["ReadCount"].toInt();
                    if(sparg.readCount<1)
                        sparg.readCount = 1;
                }
                sparg.readSleep = 100;
                if(!atts["ReadSleep"].isEmpty()){
                    sparg.readSleep = atts["ReadSleep"].toInt();
                    if(sparg.readSleep<10)
                        sparg.readSleep = 10;
                }

                sparg.timePush = 10;
                if(!atts["TimePush"].isEmpty()){
                    sparg.timePush = atts["TimePush"].toInt();
                    if(sparg.timePush<10){
                        sparg.timePush = 10;
                    }
                }
//                qInfo()<<"atts_size="<<atts.size();
//                qInfo()<<"add_serial_port="<<sparg.id;
                commdef.spdefs.insert(sparg.id,sparg);
            }
            if("nettype"==e.tagName()){
                QMap<QString,QString> atts;
                atts["id"] = "";
                atts["type"] = "";
                atts["ptype"] = "";
                atts["name"] = "";
                atts["ip"] = "";
                atts["port"] = "";
                atts["readheart"] = "";
                atts["TimePush"] = "";
                //
                readNodeAtts(atts,e);

                NetPortArg nparg;
                nparg.id = atts["id"].toInt();
                nparg.type = atts["type"].toInt();
                nparg.ptype = atts["ptype"].toInt();
                nparg.name = atts["name"];
                nparg.ip = atts["ip"];
                nparg.port = atts["port"].toInt();
                nparg.readHeartTime = false;
                if(!atts["readheart"].isEmpty()){
                    nparg.readHeartTime = atts["readheart"].toInt()>0?true:false;
                }
                nparg.timePush = 10;
                if(!atts["TimePush"].isEmpty()){
                    nparg.timePush = atts["TimePush"].toInt();
                    if(nparg.timePush<10){
                        nparg.timePush = 10;
                    }
                }
//                qInfo()<<"atts_size="<<atts.size();
//                qInfo()<<"add_net_port="<<nparg.id;
                commdef.netdefs.insert(nparg.id,nparg);
            }
            if("protocol"==e.tagName()){
                QMap<QString,QString> atts;
                atts["id"] = "";
                atts["file"] = "";
                atts["downcmd"] = "";
                atts["upcmd"] = "";
                atts["totalcallsleep"] = "";
                atts["allYXState"] = "";
                atts["YXGetValStart"] = "";
                atts["YXGetValEnd"] = "";
                atts["oldState"] = "";
                atts["ykSetVal"] = "";
                atts["retValNull"] = "";
                //
                readNodeAtts(atts,e);

                ProtocolDef pdef;
                pdef.id = atts["id"].toInt();
                pdef.jsfile = atts["file"];
                pdef.downType = atts["downcmd"].toInt();
                pdef.upType = atts["upcmd"].toInt();
                pdef.totalcallsleep = 100;
                if(!atts["totalcallsleep"].isEmpty()){
                    pdef.totalcallsleep = atts["totalcallsleep"].toInt();
                }
                pdef.oldState = 0;
                if(!atts["oldState"].isEmpty()){
                    pdef.oldState = atts["oldState"].toInt();
                }
                pdef.ykSetVal = 0;
                if(!atts["ykSetVal"].isEmpty()){
                    pdef.ykSetVal = atts["ykSetVal"].toInt();
                }
                pdef.allYXState = 0;
                if(!atts["allYXState"].isEmpty()){
                    pdef.allYXState = atts["allYXState"].toInt();
                }
                pdef.YXGetValStart = 0;
                if(!atts["YXGetValStart"].isEmpty()){
                    pdef.YXGetValStart = atts["YXGetValStart"].toInt();
                }
                pdef.YXGetValEnd = 7;
                if(!atts["YXGetValEnd"].isEmpty()){
                    pdef.YXGetValEnd = atts["YXGetValEnd"].toInt();
                }
                pdef.retValNull = 0;
                if(!atts["retValNull"].isEmpty()){
                    pdef.retValNull = atts["retValNull"].toInt();
                }
//                qInfo()<<"atts_size="<<atts.size();
//                qInfo()<<"add_protocol_def="<<pdef.id;
                commdef.protodefs.insert(pdef.id,pdef);
            }
            if("gather"==e.tagName()){
                QMap<QString,QString> atts;
                atts["id"] = "";
                atts["gtype"] = "";
                atts["dtype"] = "";
                atts["name"] = "";
                atts["prototype"] = "";
                atts["hexflag"] = "";
                atts["controlsleep"] = "";
                //
                readNodeAtts(atts,e);

                Gather gather;
                gather.atts.id = atts["id"].toInt();
                gather.atts.type = atts["gtype"].toInt();
                gather.atts.routeID = atts["dtype"].toInt();
                gather.atts.protolID = atts["prototype"].toInt();
                gather.atts.name = atts["name"];
                gather.atts.desc = atts["desc"];
                gather.atts.controlSleep = 50;
                if(!atts["controlsleep"].isEmpty()){
                    gather.atts.controlSleep  = atts["controlsleep"].toInt();
                }
                if (e.hasChildNodes())
                {
                    QDomNode n1 = e.firstChild();
                    while(!n1.isNull()) {
                        QDomElement e1 = n1.toElement(); // try to convert the node to an element.
                        if(!e1.isNull()) {
//                            qInfo() << e1.tagName() <<"#"; // the node really is an element.
                            if("P"==e1.tagName()){
                                QMap<QString,QString> atts_;
                                atts_["id"] = "";
                                atts_["type"] = "";
                                atts_["addr"] = "";
                                atts_["ip"] = "";
                                atts_["upinterval"] = "";
                                //
                                readNodeAtts(atts_,e1);

                                PInfo pinfo;
                                pinfo.id = atts_["id"].toInt();
                                pinfo.type = (PType)atts_["type"].toInt();
                                pinfo.addr = atts_["addr"].toInt();
                                pinfo.ipStr = "127.0.0.1";
                                if(!atts_["ip"].isEmpty()){
                                    pinfo.ipStr = atts_["ip"];
                                }
                                pinfo.value = 0.0;
                                pinfo.ipLong = PFunc::getIPLFromStr(pinfo.ipStr);
                                pinfo.sendTime = 0;
                                pinfo.updateTime = 0;
                                pinfo.upInterval = 1;
                                if(!atts_["upinterval"].isEmpty()){
                                    pinfo.upInterval = atts_["upinterval"].toInt();
                                    if( pinfo.upInterval <1)
                                        pinfo.upInterval  = 1;
                                }
//                                qInfo()<<"atts_size="<<atts_.size();
//                                qInfo()<<"add_g_p="<<gather.atts.id<<","<<pinfo.id;
                                gather.pinfos.append(pinfo);
                            }
                        }
                        n1 = n1.nextSibling();
                    }
                }
//                qInfo()<<"atts_size="<<atts.size();
//                qInfo()<<"add_gather_id="<<gather.atts.id<<","<<atts["id"]<<","<<atts["id"].toInt();
                commdef.gathers.append(gather);
            }
            if (e.hasChildNodes() && QDomNode::TextNode==n.firstChild().nodeType())
            {
//                qInfo() << n.firstChild().nodeValue()<< "\n";
            }
            if (!e.parentNode().isNull())
            {
//                qInfo()<<"parent="<<e.parentNode().toElement().tagName();
            }
        }
        n = n.nextSibling();
    }
}

void XMLRead::readGMap(QList<PMap> &pmaps,QString filePath)
{
    QDomElement docElem;
    if(readXml(filePath,docElem)){
        readGMapDom(pmaps,docElem);
    }
}

void XMLRead::readGMapDom(QList<PMap> &pmaps,QDomElement docElem)
{
    QDomNode n = docElem.firstChild();
    while(!n.isNull()) {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        if(!e.isNull()) {
//            qInfo() << e.tagName() <<"$"; // the node really is an element.
            if("map"==e.tagName()){
                if (e.hasChildNodes())
                {
                    PMap pmap_;
                    QDomNode n1 = e.firstChild();
                    while(!n1.isNull()) {
                        QDomElement e1 = n1.toElement(); // try to convert the node to an element.
                        if(!e1.isNull()) {
//                            qInfo() << e1.tagName() <<"#"; // the node really is an element.
                            if("from"==e1.tagName()){
                                QMap<QString,QString> atts;
                                atts["gid"] = "";
                                atts["pid"] = "";
                                atts["ptype"] = "";
                                readNodeAtts(atts,e1);
                                pmap_.pfrom.gid = atts["gid"].toInt();
                                pmap_.pfrom.pid = atts["pid"].toInt();
                                pmap_.pfrom.ptype = static_cast<PType>(atts["ptype"].toInt());
                            }
                            if("to"==e1.tagName()){
                                QMap<QString,QString> atts;
                                atts["pid"] = "";
                                atts["ptype"] = "";
                                readNodeAtts(atts,e1);
                                pmap_.pto.pid = atts["pid"].toInt();
                                pmap_.pto.ptype = static_cast<PType>(atts["ptype"].toInt());
                                pmap_.pto.value = 0.0;
                            }
                        }
                        n1 = n1.nextSibling();
                    }
                    pmaps.append(pmap_);
                }
            }
        }
        n = n.nextSibling();
    }
}

void XMLRead::readDevs(QList<Dev> &devs, CalculateIDS &ids,QString filePath )
{
    QDomElement docElem;
    if(readXml(filePath,docElem)){
        readDevDom(devs,ids,docElem);
    }
}

void XMLRead::readDevDom(QList<Dev> &devs,CalculateIDS &ids,QDomElement docElem)
{
    QDomNode n = docElem.firstChild();
    while(!n.isNull()) {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        if(!e.isNull()) {
//            qInfo() << e.tagName() <<"$"; // the node really is an element.
            if("IDcalculate"==e.tagName()){
                QMap<QString,QString> atts;
                atts["regionID"] = "";
                atts["routeID"] = "";
                atts["stationID"] = "";
                atts["platformID"] = "";
                atts["CalculateFlag"] = "";
                readNodeAtts(atts,e);
                if(!atts["regionID"].isEmpty())
                    ids.regionID = atts["regionID"].toInt();
                if(!atts["routeID"].isEmpty())
                    ids.routeID = atts["routeID"].toInt();
                if(!atts["stationID"].isEmpty())
                    ids.stationID = atts["stationID"].toInt();
                if(!atts["platformID"].isEmpty())
                    ids.platformID = atts["platformID"].toInt();
                if(!atts["CalculateFlag"].isEmpty())
                    ids.calculateFlag = atts["CalculateFlag"].toInt()>0?true:false;
            }
            if("dev"==e.tagName()){
                QMap<QString,QString> atts;
                atts["devID"] = "";
                atts["devType"] = "";
                atts["parentID"] = "";
                atts["pdevType"] = "";
                atts["name"] = "";
                atts["desc"] = "";
                atts["clientOrder"] = "";
                atts["clientType"] = "";

                readNodeAtts(atts,e);
                Dev myDev;
                myDev.devInfo.devID = atts["devID"].toInt();
                myDev.devInfo.devType = (DevType)atts["devType"].toInt();
                myDev.devInfo.pDevID = atts["parentID"].toInt();
                myDev.devInfo.pdevType = (DevType)atts["pdevType"].toInt();
                myDev.devInfo.name = atts["name"];
                myDev.devInfo.desc = atts["desc"];
                myDev.devInfo.clientOrder = 1;
                if(!atts["clientOrder"].isEmpty())
                    myDev.devInfo.clientOrder = atts["clientOrder"].toInt();
                myDev.devInfo.clientType = 1;
                if(!atts["clientType"].isEmpty())
                    myDev.devInfo.clientType = atts["clientType"].toInt();
                if(ids.calculateFlag){
                    myDev.devInfo.devID = combDevID(myDev.devInfo.devID, myDev.devInfo.devType, ids);
                    myDev.devInfo.pDevID = combDevID(myDev.devInfo.pDevID, myDev.devInfo.pdevType, ids);
                }
                if (e.hasChildNodes())
                {
                    QDomNode n1 = e.firstChild();
                    while(!n1.isNull()) {
                        QDomElement e1 = n1.toElement(); // try to convert the node to an element.
                        if(!e1.isNull()) {
//                            qInfo() << e1.tagName() <<"#"; // the node really is an element.
                            if("pInfo"==e1.tagName()){
                                QMap<QString,QString> PInfo_atts;
                                PInfo_atts["pID"] = "";
                                PInfo_atts["pType"] = "";
                                PInfo_atts["name"] = "";
                                PInfo_atts["desc"] = "";
                                PInfo_atts["orgType"] = "";
                                PInfo_atts["defaultVal"] = "";
                                PInfo_atts["ratio"] = "";
                                PInfo_atts["base"] = "";
                                PInfo_atts["changeRecord"] = "";
                                PInfo_atts["clientOrder"] = "";
                                PInfo_atts["clientType"] = "";
                                PInfo_atts["UpLoopTime"] = "";

                                readNodeAtts(PInfo_atts,e1);
                                PInfoDev _pinfo;
                                _pinfo.ratio = 1.0;
                                _pinfo.base = 0.0;
                                _pinfo.defVal = 0.0;
                                _pinfo.ValChange = false;
                                _pinfo.changeRecord = true;
                                _pinfo.markT = 0;

                                _pinfo.pID = PInfo_atts["pID"].toInt();
                                _pinfo.pType = static_cast<PType>(PInfo_atts["pType"].toInt());
                                _pinfo.name = PInfo_atts["name"];
                                _pinfo.desc = PInfo_atts["desc"];

                                _pinfo.orgType = static_cast<OrgType>(PInfo_atts["orgType"].toInt());
                                if(!PInfo_atts["defaultVal"].isEmpty())
                                    _pinfo.defVal = static_cast<float>(PInfo_atts["defaultVal"].toFloat());

                                if(!PInfo_atts["ratio"].isEmpty())
                                    _pinfo.ratio = static_cast<float>(PInfo_atts["ratio"].toFloat());
                                if(!PInfo_atts["base"].isEmpty())
                                    _pinfo.base = static_cast<float>(PInfo_atts["base"].toFloat());
                                if(!PInfo_atts["changeRecord"].isEmpty())
                                    _pinfo.changeRecord = static_cast<bool>(PInfo_atts["changeRecord"].toInt());

                                _pinfo.clientOrder = 1;
                                if(!PInfo_atts["clientOrder"].isEmpty())
                                    _pinfo.clientOrder = PInfo_atts["clientOrder"].toInt();
                                _pinfo.clientType = 1;
                                if(!PInfo_atts["clientType"].isEmpty())
                                    _pinfo.clientType = PInfo_atts["clientType"].toInt();
                                if(ids.calculateFlag){
                                    _pinfo.pID = combPID(_pinfo.pID, _pinfo.pType);
                                }
                                _pinfo.upLoopTime = 3;
                                if(!PInfo_atts["UpLoopTime"].isEmpty()){
                                    _pinfo.upLoopTime = PInfo_atts["UpLoopTime"].toInt();
                                    if(_pinfo.upLoopTime < 3)
                                        _pinfo.upLoopTime = 3;
                                }
                                myDev.pInfo.append(_pinfo);
                            }
                        }
                        n1 = n1.nextSibling();
                    }
                }
                devs.append(myDev);
            }
        }
        n = n.nextSibling();
    }
}

void XMLRead::readPMaps(QList<PMapDev> &pmaps, CalculateIDS ids, QString filePath )
{
    QDomElement docElem;
    if(readXml(filePath,docElem)){
        readPMapDom(pmaps,ids,docElem);
    }
}

void XMLRead::readPMapDom(QList<PMapDev> &pmaps,CalculateIDS ids,QDomElement docElem)
{
    QDomNode n = docElem.firstChild();
    while(!n.isNull()) {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        if(!e.isNull()) {
//            qInfo() << e.tagName() <<"$"; // the node really is an element.
            if("map"==e.tagName()){
                if (e.hasChildNodes())
                {
                    PMapDev pmap_;
                    QDomNode n1 = e.firstChild();
                    while(!n1.isNull()) {
                        QDomElement e1 = n1.toElement(); // try to convert the node to an element.
                        if(!e1.isNull()) {
//                            qInfo() << e1.tagName() <<"#"; // the node really is an element.
                            if("from"==e1.tagName()){
                                QMap<QString,QString> from_atts;
                                from_atts["ip"] = "";
                                from_atts["ID"] = "";
                                from_atts["pType"] = "";

                                readNodeAtts(from_atts,e1);
                                pmap_.from.ipStr = from_atts["ip"];
                                pmap_.from.ipLong = PFunc::ipToInt(pmap_.from.ipStr);
                                pmap_.from.pID = from_atts["ID"].toInt();
                                pmap_.from.pType = static_cast<PType>(from_atts["pType"].toInt());
                            }
                            if("to"==e1.tagName()){
                                QMap<QString,QString> to_atts;
                                to_atts["devID"] = "";
                                to_atts["devType"] = "";
                                to_atts["pID"] = "";
                                to_atts["pType"] = "";
                                readNodeAtts(to_atts,e1);
                                pmap_.to.devID = to_atts["devID"].toInt();
                                pmap_.to.devType = static_cast<DevType>(to_atts["devType"].toInt());
                                pmap_.to.pID = to_atts["pID"].toInt();
                                pmap_.to.pType = static_cast<PType>(to_atts["pType"].toInt());
                                if(ids.calculateFlag){
                                    pmap_.to.devID = combDevID(pmap_.to.devID, pmap_.to.devType, ids);
                                    pmap_.to.pID = combPID(pmap_.to.pID, pmap_.to.pType);
                                }
                            }
                        }
                        n1 = n1.nextSibling();
                    }
                    pmaps.append(pmap_);
                }
            }
        }
        n = n.nextSibling();
    }
}

void XMLRead::readPlans(QList<Plan> &plans, CalculateIDS ids, QString filePath )
{
    QDomElement docElem;
    if(readXml(filePath,docElem)){
        readPlanDom(plans,ids,docElem);
    }
}

void XMLRead::readPlanDom(QList<Plan> &pmaps,CalculateIDS ids,QDomElement docElem)
{
    QDomNode n = docElem.firstChild();
    while(!n.isNull()) {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        if(!e.isNull()) {
//            qInfo() << e.tagName() <<"$"; // the node really is an element.
            if("plan"==e.tagName()){
                QMap<QString,QString> plan_atts;
                plan_atts["planID"] = "";
                plan_atts["planType"] = "";
                plan_atts["desc"] = "";
                plan_atts["planEffect"] = "";

                readNodeAtts(plan_atts,e);
                Plan _plan;
                _plan.planAtt.planID = plan_atts["planID"].toInt();
                _plan.planAtt.planType = static_cast<PlanType>(plan_atts["planType"].toInt());
                _plan.planAtt.desc = plan_atts["desc"];
                _plan.planAtt.planEffect = 0;
                if(!plan_atts["planEffect"].isEmpty()){
                    _plan.planAtt.planEffect = plan_atts["planEffect"].toInt();
                    if(_plan.planAtt.planEffect<0)
                        _plan.planAtt.planEffect = 0;
                }

                if (e.hasChildNodes())
                {
                    QDomNode n1 = e.firstChild();
                    while(!n1.isNull()) {
                        QDomElement e1 = n1.toElement(); // try to convert the node to an element.
                        if(!e1.isNull()) {
//                            qInfo() << e1.tagName() <<"#"; // the node really is an element.
                            if("DTime"==e1.tagName()){
                                QMap<QString,QString> dt_atts;
                                dt_atts["year"] = "";
                                dt_atts["month"] = "";
                                dt_atts["day"] = "";
                                dt_atts["hour"] = "";
                                dt_atts["min"] = "";
                                dt_atts["wday"] = "";

                                readNodeAtts(dt_atts,e1);
                                _plan.ptime.myear = ("*"==dt_atts["year"]?-1:dt_atts["year"].toInt());
                                _plan.ptime.mmon = ("*"==dt_atts["month"]?-1:dt_atts["month"].toInt());
                                _plan.ptime.mday = ("*"==dt_atts["day"]?-1:dt_atts["day"].toInt());
                                _plan.ptime.mhour = ("*"==dt_atts["hour"]?-1:dt_atts["hour"].toInt());
                                _plan.ptime.mmin = ("*"==dt_atts["min"]?-1:dt_atts["min"].toInt());
                                if(!dt_atts["wday"].isEmpty()){
                                    _plan.ptime.wday = ("*"==dt_atts["wday"]?-1:dt_atts["wday"].toInt());
                                }else{
                                    _plan.ptime.wday = -1;
                                }
                                _plan.ptime.flag = false;
                                //校验 年 月 日 时 分 是否越限
                                //your code
                            }
                            if("Interval"==e1.tagName()){
                                QMap<QString,QString> sleep_atts;
                                sleep_atts["sleepT"] = "";
                                readNodeAtts(sleep_atts,e1);
                                _plan.psleep.interval = sleep_atts["sleepT"].toInt();
                                if (_plan.psleep.interval < 1)
                                {
                                    _plan.psleep.interval = 1;
                                }
                                _plan.psleep.tt = 10;
                                if (e1.hasChildNodes())
                                {
                                    QDomNode n2 = e1.firstChild();
                                    {
                                        while(!n2.isNull()) {
                                            QDomElement e2 = n2.toElement(); // try to convert the node to an element.
                                            if(!e2.isNull()) {
                                                if("TV"==e2.tagName()){
                                                    QMap<QString,QString> tv_atts;
                                                    tv_atts["startHour"] = "";
                                                    tv_atts["endHour"] = "";
                                                    tv_atts["startMin"] = "";
                                                    tv_atts["endMin"] = "";
                                                    readNodeAtts(tv_atts,e2);
                                                    TVProperty tv_;

                                                    tv_.startHour = 0;
                                                    if (!tv_atts["startHour"].isEmpty())
                                                    {
                                                        tv_.startHour = tv_atts["startHour"].toInt();
                                                        if (tv_.startHour < 0)
                                                            tv_.startHour = 0;
                                                        if (tv_.startHour > 23)
                                                            tv_.startHour = 23;
                                                    }
                                                    tv_.endHour = 24;
                                                    if (!tv_atts["endHour"].isEmpty())
                                                    {
                                                        tv_.endHour = tv_atts["endHour"].toInt();
                                                        if (tv_.endHour < 0)
                                                            tv_.endHour = 0;
                                                        if (tv_.endHour > 23)
                                                            tv_.endHour = 23;
                                                    }
                                                    //2018-05-25 for shanghai
                                                    tv_.startMin = 0;
                                                    if (!tv_atts["startMin"].isEmpty())
                                                    {
                                                        tv_.startMin = tv_atts["startMin"].toInt();
                                                        if (tv_.startMin < 0)
                                                            tv_.startMin = 0;
                                                        if (tv_.startMin > 59)
                                                            tv_.startMin = 59;
                                                    }
                                                    tv_.endMin = 0;
                                                    if (!tv_atts["endMin"].isEmpty())
                                                    {
                                                        tv_.endMin = tv_atts["endMin"].toInt();
                                                        if (tv_.endMin < 0)
                                                            tv_.endMin = 0;
                                                        if (tv_.endMin > 59)
                                                            tv_.endMin = 59;
                                                    }
                                                    tv_.allStartMin = 60 * tv_.startHour + tv_.startMin;
                                                    tv_.allEndMin = 60 * tv_.endHour + tv_.endMin;
                                                    _plan.timeVS.push_back(tv_);
                                                }
                                            }
                                            n2 = n2.nextSibling();
                                        }
                                    }
                                }
                            }
                            if("condition"==e1.tagName()){
                                if (e1.hasChildNodes())
                                {
                                    QDomNode n2 = e1.firstChild();
                                    while(!n2.isNull()) {
                                        QDomElement e2 = n2.toElement(); // try to convert the node to an element.
                                        if(!e2.isNull()) {
                                            if("TV"==e2.tagName()){
                                                QMap<QString,QString> tv_atts;
                                                tv_atts["startHour"] = "";
                                                tv_atts["endHour"] = "";
                                                tv_atts["startMin"] = "";
                                                tv_atts["endMin"] = "";
                                                readNodeAtts(tv_atts,e2);
                                                TVProperty tv_;

                                                tv_.startHour = 0;
                                                if (!tv_atts["startHour"].isEmpty())
                                                {
                                                    tv_.startHour = tv_atts["startHour"].toInt();
                                                    if (tv_.startHour < 0)
                                                        tv_.startHour = 0;
                                                    if (tv_.startHour > 23)
                                                        tv_.startHour = 23;
                                                }
                                                tv_.endHour = 24;
                                                if (!tv_atts["endHour"].isEmpty())
                                                {
                                                    tv_.endHour = tv_atts["endHour"].toInt();
                                                    if (tv_.endHour < 0)
                                                        tv_.endHour = 0;
                                                    if (tv_.endHour > 23)
                                                        tv_.endHour = 23;
                                                }
                                                //2018-05-25 for shanghai
                                                tv_.startMin = 0;
                                                if (!tv_atts["startMin"].isEmpty())
                                                {
                                                    tv_.startMin = tv_atts["startMin"].toInt();
                                                    if (tv_.startMin < 0)
                                                        tv_.startMin = 0;
                                                    if (tv_.startMin > 59)
                                                        tv_.startMin = 59;
                                                }
                                                tv_.endMin = 0;
                                                if (!tv_atts["endMin"].isEmpty())
                                                {
                                                    tv_.endMin = tv_atts["endMin"].toInt();
                                                    if (tv_.endMin < 0)
                                                        tv_.endMin = 0;
                                                    if (tv_.endMin > 59)
                                                        tv_.endMin = 59;
                                                }
                                                tv_.allStartMin = 60 * tv_.startHour + tv_.startMin;
                                                tv_.allEndMin = 60 * tv_.endHour + tv_.endMin;
                                                _plan.timeVS.push_back(tv_);
                                            }
                                            if("p"==e2.tagName()){
                                                QMap<QString,QString> cp_atts;
                                                cp_atts["cid"] = "";
                                                cp_atts["devID"] = "";
                                                cp_atts["devType"] = "";
                                                cp_atts["pID"] = "";
                                                cp_atts["pType"] = "";
                                                cp_atts["value"] = "";
                                                cp_atts["compare"] = "";
                                                readNodeAtts(cp_atts,e2);
                                                PlanCondition _planConD;
                                                _planConD.cID = cp_atts["cid"].toInt();
                                                _planConD.devID = cp_atts["devID"].toInt();
                                                _planConD.devType = (DevType)cp_atts["devType"].toInt();
                                                _planConD.pID = cp_atts["pID"].toInt();
                                                _planConD.pType = (PType)cp_atts["pType"].toInt();
                                                _planConD.val = static_cast<float>(cp_atts["value"].toFloat());
                                                _planConD.compare = (CompareType)cp_atts["compare"].toInt();
                                                //
                                                if(ids.calculateFlag){
                                                    _planConD.devID = combDevID(_planConD.devID, _planConD.devType, ids);
                                                    _planConD.pID = combPID(_planConD.pID, _planConD.pType);
                                                }
                                                _plan.plancons.push_back(_planConD);
                                            }
                                        }
                                        n2 = n2.nextSibling();
                                    }
                                }
                            }
                            if("StartPlan"==e1.tagName()){
                                QMap<QString,QString> pfs_atts;
                                pfs_atts["timedelay"] = "";
                                readNodeAtts(pfs_atts,e1);
                                _plan.pFStart.execFlag = false;
                                _plan.pFStart.waitSec = 10;
                                if (!pfs_atts["timedelay"].isEmpty())
                                {
                                    _plan.pFStart.waitSec = pfs_atts["timedelay"].toInt();
                                    if (_plan.pFStart.waitSec < 10)
                                        _plan.pFStart.waitSec = 10;
                                }
                            }
                            if("CMD"==e1.tagName()){
                                QMap<QString,QString> cmd_atts;
                                cmd_atts["devID"] = "";
                                cmd_atts["devType"] = "";
                                cmd_atts["pID"] = "";
                                cmd_atts["pType"] = "";
                                cmd_atts["exeType"] = "";
                                cmd_atts["value"] = "";
                                cmd_atts["waitT"] = "";
                                cmd_atts["timedelay"] = "";
                                cmd_atts["desc"] = "";
                                readNodeAtts(cmd_atts,e1);
                                PlanCMD _plancmd;
                                _plancmd.devID = cmd_atts["devID"].toInt();
                                _plancmd.devType = (DevType)cmd_atts["devType"].toInt();
                                _plancmd.pID = cmd_atts["pID"].toInt();
                                _plancmd.pType = (PType)cmd_atts["pType"].toInt();
                                _plancmd.exeType = (ExeType)cmd_atts["exeType"].toInt();
                                _plancmd.val = static_cast<float>(cmd_atts["value"].toFloat());
                                _plancmd.waitT = cmd_atts["waitT"].toInt();
                                _plancmd.timedelay = cmd_atts["timedelay"].toInt();
                                _plancmd.desc = cmd_atts["desc"];
                                //_plancmd.desc = UTF_82ASCII(cmd_atts["desc"]);
                                //
                                if(ids.calculateFlag){
                                    _plancmd.devID = combDevID(_plancmd.devID, _plancmd.devType, ids);
                                    _plancmd.pID = combPID(_plancmd.pID, _plancmd.pType);
                                }
                                _plan.planCmds.push_back(_plancmd);
                            }
                        }
                        n1 = n1.nextSibling();
                    }
                }
                pmaps.append(_plan);
            }
        }
        n = n.nextSibling();
    }
}

bool XMLRead::readXml(QString filePath,QDomElement &docElem)
{
    bool ret = false;
    try{
        if (!filePath.isEmpty()) {
            QFile file(filePath);
            if (file.open(QIODevice::ReadOnly)) {
                QDomDocument document;
                if (document.setContent(&file)) {
                    docElem = document.documentElement();
                    ret = true;
                }
                file.close();
            }else{
                qWarning()<<QString("open %1 fail!").arg(filePath);
            }
        }
    }catch(...){
        qDebug()<<QString("open %1 error,make sure the file is right format!").arg(filePath);
    }

    return ret;
}

void XMLRead::readNodeAtts(QMap<QString,QString> &atts,QDomElement e)
{
    try{
        if (e.hasAttributes())
        {
    //        QStringList attributes;
            for (int i = 0; i < e.attributes().count(); i++) {
                QDomNode attribute = e.attributes().item(i);
    //            attributes << attribute.nodeName() + "="
    //                       +attribute.nodeValue();
                atts[attribute.nodeName().trimmed()] = attribute.nodeValue().trimmed();
             }
    //        QString attributeStr = attributes.join(" ");
    //        qInfo() << attributeStr << "$";
        }
    }catch(...){
        qDebug() << "read attributes is error!";
    }
}

unsigned long long XMLRead::combDevID(unsigned long long _devID, DevType _devType, CalculateIDS _ids)
{
    return ((unsigned long long)(_ids.regionID) << 26)
        | ((unsigned long long)(_ids.routeID) << 20)
        | ((unsigned long long)(_ids.stationID) << 14)
        | ((unsigned long long)(_ids.platformID) << 11)
        | ((unsigned long long)(_devType) << 8)
        | _devID;
};

unsigned int XMLRead::combPID(unsigned int _pID, PType _pType)
{
    return ((unsigned int )(_pType) << 24) | _pID;
};

//unsigned long long XMLRead::decompDevID(unsigned long long _devID, DevType _devType, CalculateIDS _ids)
//{
//    return (((unsigned long long)(_devID) << 56)>>56);
//};

//unsigned int XMLRead::decompPID(unsigned int _pID, PType _pType)
//{
//    return (((unsigned int)(_pType) << 8)>>8);
//};

