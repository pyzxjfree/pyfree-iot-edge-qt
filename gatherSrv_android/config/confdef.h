#ifndef CONFDEF_H
#define CONFDEF_H

#include <QString>
#include <QVector>
#include <QList>
#include <QMap>

struct MyConf
{
    MyConf()
        : ipCleintJson("127.0.0.1")
        , portClientJson(60009)
        , listenPortPlayer(60004)
        , portLocal(60008)
        //disk
        , diskSymbol('D')
        , freeSizeLimit(10000)
        , gDataDir("gadata")
        //path
        , gatherPath("gather.xml")
        , gmapPath("gmap.xml")
        , devPath("dev.xml")
        , pmapPath("pmap.xml")
        , planPath("plan.xml")
        //area desc
        , areaId(1)
        , areaType(1)
        , areaName("zhsye")
        , areaDesc("zhsye")
        //func
        , yunFunc(0)
        , mcsFunc(0)
        , timeUpVirtual(0)
        , viewFunc(1)
    {

    };

    //链接第三方信息Json
    QString ipCleintJson;
    int     portClientJson;
    //侦听播放终端链接端口
    int     listenPortPlayer;
    //侦听中控终端链接端口
    int     portLocal;
    //信息存储盘符
    char    diskSymbol;
    //存储路径所在磁盘剩余空间(MB)
    int     freeSizeLimit;//MB
    //存储路径
    QString gDataDir;

    //xml_path
    QString gatherPath;
    QString gmapPath;
    QString devPath;
    QString pmapPath;
    QString planPath;
    //area id
    int areaId;
    int areaType;
    QString areaName;
    QString areaDesc;
    //func
    int yunFunc;
    int mcsFunc;
    int timeUpVirtual;
    int viewFunc;
};

enum PType
{
    OnYX = 1,   //遥信
    OnYC = 2,   //遥测
    OnYXS = 3,  //组合遥信
    OnYCS = 4,  //组合遥测
    PTDef = 0
};

struct SerialPortArg
{
    int     id;
    QString name;
    int     BaudRate;
    int     DataBit;
    QString Parity;
    int     StopBit;
    int     readCount;
    int     readSleep;
    int     timePush;
};

struct NetPortArg
{
    int     id;
    int     type;
    int     ptype;
    QString name;
    QString ip;
    int     port;
    bool    readHeartTime;
    int timePush;
};

struct ProtocolDef
{
    int     id;
    QString jsfile;
    int     downType;
    int     upType;
    int     totalcallsleep;
    int     allYXState;
    int     YXGetValStart;
    int     YXGetValEnd;
    int     oldState;
    int     ykSetVal;
    int     retValNull;
};

struct PInfo
{
    int         id;
    PType       type;
    int         addr;
    float       value;
    QString     ipStr;
    ulong       ipLong;
    uint        updateTime;
    uint        sendTime;
    int         upInterval;
};

struct GatherAtts
{
    int     id;
    int     type;
    int     routeID;
    int     protolID;
    QString name;
    QString desc;
    int     controlSleep;
};

struct Gather
{
    GatherAtts atts;
    QList<PInfo> pinfos;
};

struct  Transmit
{
    int id;
    int trantype;
    int dtype;
    int protocoltype;
};

struct ComManagerDef
{
    QList<Transmit>         trans;
    QMap<int,SerialPortArg> spdefs;
    QMap<int,NetPortArg>    netdefs;
    QMap<int,ProtocolDef>   protodefs;
    QList<Gather>           gathers;
};

struct PFrom
{
    int gid;
    int pid;
    PType ptype;
};

struct PTo
{
    int pid;
    PType ptype;
    float value;
};

struct PMap
{
    PFrom pfrom;
    PTo pto;
};


struct CalculateIDS {
    CalculateIDS()
        : regionID(1)
        , routeID(1)
        , stationID(1)
        , platformID(1)
        , calculateFlag(false)
    {

    };
    int regionID;
    int routeID;
    int stationID;
    int platformID;
    bool calculateFlag;
};

//设备配置
enum DevType
{
    Region  =1,//区域
    Platform=2,//站台
    Entity  =3,//实体
    DevTDev=0
};

enum OrgType
{
    OnEntity=1,
    OnVirtual,
    OnCalculate,
    OrgTDef=0
};

struct DevProPerty
{
    //设备编号 设备类型
    long long   devID;
    DevType     devType;
    //父设备编号 父设备类型
    long long   pDevID;
    DevType     pdevType;
    QString     name;
    QString     desc;
    int         clientOrder;
    int         clientType;//针对不同类型客户端推送
};

struct PInfoDev
{
    unsigned int pID;
    PType       pType;
    QString     name;
    QString     desc;
    OrgType     orgType;
    float       defVal;
    bool        ValChange;
    bool        changeRecord;
    unsigned int markT;
    //Func func;
    float       ratio;
    float       base;
    int         clientOrder;//是否客户端需求
    int         clientType;//针对不同类型客户端推送
    int         upLoopTime;
};

struct Dev
{
    DevProPerty     devInfo;
    QList<PInfoDev> pInfo;
};

class DevList
{
    DevList(): dev(Dev()){};
    DevList(Dev _dev): dev(_dev){};
    //查找 遍历 设置
    private:
    Dev dev;
    QList<DevList> childs;
};

/*
设备集
*/
//映射配置
struct PFromDev
{
    QString         ipStr;
    unsigned long   ipLong;
    unsigned int    pID;
    PType           pType;
};

struct PToDev
{
    long long   devID;
    DevType     devType;
    unsigned int pID;
    PType       pType;
};

struct PMapDev
{
    PFromDev from;
    PToDev to;
};

//计划配置
enum PlanType
{
    OnTime=1,//定时
    OnPoll,//轮询
    OnCondition,//条件
    PlanTDef=0
};

enum ExeType
{
    OnGet =   1,//查询
    OnSet =   2,//设值
    OnTran=   3 ,//转发
    ExeTDef=  0
};

struct PlanTime
{
    int myear;
    int mmon;
    int mday;
    int mhour;
    int mmin;
    int wday;
    bool flag;
};

struct PlanSleep
{
    unsigned int interval;
    unsigned int tt;
};

struct PlanProperty
{
    int planID;
    PlanType planType;
    QString desc;
    int planEffect;
};

enum CompareType
{
    OnEqual=1,
    OnMore,
    OnLess,
    ComTDef=0
};

struct PlanCondition
{
    int cID;
    long long devID;
    DevType devType;
    unsigned int pID;
    PType pType;
    float val;
    CompareType compare;
};

//开机任务
struct PlanForStart
{
    PlanForStart() : waitSec(10), execFlag(false)
    {
    };
    unsigned int waitSec;
    bool execFlag;
};
//条件策略的总条件
struct TVProperty
{
    unsigned int startHour;
    unsigned int startMin;
    unsigned int endHour;
    unsigned int endMin;

    unsigned int allStartMin;
    unsigned int allEndMin;
};

struct PlanCMD
{
    long long devID;
    DevType devType;
    unsigned int pID;
    PType pType;
    ExeType exeType;
    float val;
    int waitT;
    int timedelay;
    QString desc;
};

struct Plan
{
    PlanProperty planAtt;
    PlanTime	ptime;
    PlanSleep	psleep;
    PlanForStart pFStart;
    QList<TVProperty> timeVS;
    QList<PlanCondition> plancons;
    QList<PlanCMD> planCmds;
};

#endif // CONFDEF_H
