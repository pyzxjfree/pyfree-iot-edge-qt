#ifndef CONFIGREAD_H
#define CONFIGREAD_H

#include "confdef.h"
#include <QDomDocument>

namespace XMLRead
{
    void readAppConf(MyConf &conf, QString filePath = "appconf.xml");
    void readGatherDom(MyConf &conf,QDomElement docElem);

    void readGather(ComManagerDef &commdef,QString filePath="gather.xml");
    void readGatherDom(ComManagerDef &commdef,QDomElement docElem);

    void readGMap(QList<PMap> &pmaps,QString filePath="gmap.xml");
    void readGMapDom(QList<PMap> &pmaps,QDomElement docElem);

    void readDevs(QList<Dev> &devs, CalculateIDS &ids,QString filePath = "dev.xml");
    void readDevDom(QList<Dev> &devs,CalculateIDS &ids,QDomElement docElem);

    void readPMaps(QList<PMapDev> &pmaps, CalculateIDS ids, QString filePath = "pmap.xml");
    void readPMapDom(QList<PMapDev> &pmaps,CalculateIDS ids,QDomElement docElem);

    void readPlans(QList<Plan> &plans, CalculateIDS ids, QString filePath = "plan.xml");
    void readPlanDom(QList<Plan> &pmaps,CalculateIDS ids,QDomElement docElem);
    //
    bool readXml(QString filePath,QDomElement &docElem);
    void readNodeAtts(QMap<QString,QString> &atts,QDomElement e);
    //
    unsigned long long combDevID(unsigned long long _devID, DevType _devType, CalculateIDS _ids);
    unsigned int combPID(unsigned int _pID, PType _pType);
//    unsigned long long decompDevID(unsigned long long _devID, DevType _devType, CalculateIDS _ids);
//    unsigned int decompPID(unsigned int _pID, PType _pType);
};

#endif // CONFIGREAD_H
