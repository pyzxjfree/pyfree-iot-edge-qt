// MonitorService.cpp : 定义控制台应用程序的入口点。
//

#ifdef WIN32
#pragma warning(disable: 4995)  //for swprintf

//#include <io.h>
#include <windows.h>
#include <tchar.h>
#include <strsafe.h>
//#include <iostream>

#include "atlcomtime.h"
#pragma comment(lib, "advapi32.lib")

//#include "version.h"

int MyMain(int argc, char * argv[]);		//Actual Main function, should be defined
extern char SVCNAME[128];					//SVC Name, should be defined
extern char SVCDESC[256];					//SVC Desc, should be defined

namespace WINSVC
{
SERVICE_STATUS          gSvcStatus; 
SERVICE_STATUS_HANDLE   gSvcStatusHandle; 
HANDLE                  ghSvcStopEvent = NULL;

VOID SvcInstall(void);
VOID DoDeleteSvc(void);
VOID WINAPI SvcCtrlHandler( DWORD ); 
VOID WINAPI SvcMain( DWORD, LPTSTR * ); 

VOID ReportSvcStatus( DWORD, DWORD, DWORD );
VOID SvcInit( DWORD, LPTSTR * ); 
VOID SvcReportEvent( LPTSTR );
//BOOL ShowMenu(void);
VOID CALLBACK MainThread(HWND hwnd,UINT uMsg,UINT_PTR idEvent,DWORD dwTime);
//void WriteLog( const int iMsgType, const std::string strMsg);
HANDLE m_HThread;
DWORD m_dwThreadID;
bool g_bRun = FALSE;
}

void __cdecl _tmain(int argc, TCHAR *argv[]) 
{ 
	printf("argv[1]=%s\r\n",argv[1]);
	if( lstrcmpi( argv[1], TEXT("install")) == 0 )
	{
		WINSVC::SvcInstall();
		fflush(stdout);
		return;
	}
	else if( lstrcmpi( argv[1], TEXT("uninstall")) == 0 )
	{
		WINSVC::DoDeleteSvc();
		return;
	}
	SERVICE_TABLE_ENTRY DispatchTable[] = 
	{ 
		{ SVCNAME, (LPSERVICE_MAIN_FUNCTION) WINSVC::SvcMain }, 
		{ NULL, NULL } 
	}; 

	WINSVC::g_bRun = TRUE;
	if (!StartServiceCtrlDispatcher( DispatchTable )) 
    {
        TCHAR  svcreportStr[64] = TEXT("StartServiceCtrlDispatcher");
        WINSVC::SvcReportEvent(svcreportStr);
		printf("Main thread start successfully\r\n");
		MyMain(argc, argv);
		
		MSG msg;
		while (WINSVC::g_bRun)
		{
			//code
			if(GetMessage(&msg, 0, 0, 0))
				DispatchMessage(&msg);
			Sleep(100);
		}
		//MSG msg;
		//while (GetMessage(&msg, 0, 0, 0))
		//	DispatchMessage(&msg);
		printf("Main thread quit successfully\r\n");
	} 
} 

VOID WINSVC::SvcInstall()
{
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	TCHAR szPath[MAX_PATH];
	printf("SvcInstall begin GetModuleFileName\r\n");
	if( !GetModuleFileName( NULL, szPath, MAX_PATH ) )
	{
		printf("Install service fail (%d), %s %s %d!"
			, GetLastError(), __FILE__, __FUNCTION__, __LINE__);
		return;
	}

	// Get a handle to the SCM database. 
	printf("SvcInstall begin OpenSCManager\r\n");
	schSCManager = OpenSCManager( 
		NULL,                    // local computer
		NULL,                    // ServicesActive database 
		SC_MANAGER_ALL_ACCESS);  // full access rights 

	if (NULL == schSCManager) 
	{
		printf("Failed to open service manager (%d), %s %s %d!\r\n"
			, GetLastError(), __FILE__, __FUNCTION__, __LINE__);
		return;
	}

	// Create the service
	printf("SvcInstall begin CreateService\r\n");
	schService = CreateService( 
		schSCManager,              // SCM database 
		SVCNAME,                   // name of service 
		SVCNAME,                   // service name to display 
		SERVICE_ALL_ACCESS,        // desired access 
		SERVICE_WIN32_OWN_PROCESS, // service type 
		SERVICE_AUTO_START,      // start type 
		SERVICE_ERROR_NORMAL,      // error control type 
		szPath,                    // path to service's binary 
		NULL,                      // no load ordering group 
		NULL,                      // no tag identifier 
		NULL,                      // no dependencies 
		NULL,                      // LocalSystem account 
		NULL);                     // no password 

	if (schService == NULL) 
	{
		printf("Failed to create service (%d), %s %s %d!\r\n"
			, GetLastError(), __FILE__, __FUNCTION__, __LINE__);
		CloseServiceHandle(schSCManager);
		return;
	}
    else {
		printf(SVCDESC);
    }
	printf("\r\nSvcInstall finish and CloseServiceHandle\r\n");
	CloseServiceHandle(schService); 
	CloseServiceHandle(schSCManager);
}


//
// Purpose: 
//   Deletes a service from the SCM database
//
// Parameters:
//   None
// 
// Return value:
//   None
//
VOID WINSVC::DoDeleteSvc()
{
	SC_HANDLE schSCManager;
	SC_HANDLE schService;
	//SERVICE_STATUS ssStatus; 

	// Get a handle to the SCM database. 

	schSCManager = OpenSCManager( 
		NULL,                    // local computer
		NULL,                    // ServicesActive database 
		SC_MANAGER_ALL_ACCESS);  // full access rights 

	if (NULL == schSCManager) 
	{
		printf("Failed to open service manager (%d), %s %s %d!\r\n"
			, GetLastError(), __FILE__, __FUNCTION__, __LINE__);
		return;
	}

	// Get a handle to the service.

	schService = OpenService( 
		schSCManager,       // SCM database 
		SVCNAME,          // name of service 
		DELETE);            // need delete access 

	if (schService == NULL)
	{ 
		printf("Failed to get service (%d), %s %s %d!\r\n"
			, GetLastError(), __FILE__, __FUNCTION__, __LINE__);
		CloseServiceHandle(schSCManager);
		return;
	}

	// Delete the service.

	if (! DeleteService(schService) ) 
	{
		printf("Failed to delete service (%d), %s %s %d!\r\n"
			, GetLastError(), __FILE__, __FUNCTION__, __LINE__);
	}
    else {
		printf("Service(%s) \r\nsuccessfully removed !\r\n",SVCDESC);
    }
	CloseServiceHandle(schService); 
	CloseServiceHandle(schSCManager);
}

//
// Purpose: 
//   Entry point for the service
//
// Parameters:
//   dwArgc - Number of arguments in the lpszArgv array
//   lpszArgv - Array of strings. The first string is the name of
//     the service and subsequent strings are passed by the process
//     that called the StartService function to start the service.
// 
// Return value:
//   None.
//
VOID WINAPI WINSVC::SvcMain( DWORD dwArgc, LPTSTR *lpszArgv )
{
	// Register the handler function for the service

	gSvcStatusHandle = RegisterServiceCtrlHandler( 
		SVCNAME, 
		SvcCtrlHandler);

	if( !gSvcStatusHandle )
	{ 
        TCHAR svcReportStr[64]=TEXT("RegisterServiceCtrlHandler");
        SvcReportEvent(svcReportStr);
		return; 
	} 

	// These SERVICE_STATUS members remain as set here

	gSvcStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS; 
	gSvcStatus.dwServiceSpecificExitCode = 0;    

	// Report initial status to the SCM

	ReportSvcStatus( SERVICE_START_PENDING, NO_ERROR, 3000 );

	// Perform service-specific initialization and work.

	SvcInit( dwArgc, lpszArgv );
}

//
// Purpose: 
//   The service code
//
// Parameters:
//   dwArgc - Number of arguments in the lpszArgv array
//   lpszArgv - Array of strings. The first string is the name of
//     the service and subsequent strings are passed by the process
//     that called the StartService function to start the service.
// 
// Return value:
//   None
//
VOID WINSVC::SvcInit( DWORD dwArgc, LPTSTR *lpszArgv)
{
	// TO_DO: Declare and set any required variables.
	//   Be sure to periodically call ReportSvcStatus() with 
	//   SERVICE_START_PENDING. If initialization fails, call
	//   ReportSvcStatus with SERVICE_STOPPED.

	// Create an event. The control handler function, SvcCtrlHandler,
	// signals this event when it receives the stop control code.

	TCHAR szPath[MAX_PATH];
	GetModuleFileName( NULL, szPath, MAX_PATH );
	TCHAR drive[MAX_PATH],dir[MAX_PATH],fname[MAX_PATH],ext[MAX_PATH];
	_tsplitpath_s( szPath,drive,dir,fname,ext );
	strcpy_s( szPath, drive );
	strcat_s( szPath, dir );
	SetCurrentDirectory( szPath );

	ghSvcStopEvent = CreateEvent(
		NULL,    // default security attributes
		TRUE,    // manual reset event
		FALSE,   // not signaled
		NULL);   // no name

	if ( ghSvcStopEvent == NULL)
	{
		ReportSvcStatus( SERVICE_STOPPED, NO_ERROR, 0 );
		return;
	}
	//{
	//	int nDirSend  = 0;
	//	if(lpszArgv[1])
	//	{
	//		nDirSend = atoi(lpszArgv[1]);
	//		if(nDirSend > 2)
	//			nDirSend = 2;
	//	}
	//	CControlCenter::createInstance()->SetDir(nDirSend);
	//}	
	m_HThread = CreateThread( (LPSECURITY_ATTRIBUTES)NULL, 0, (LPTHREAD_START_ROUTINE)MainThread,	0, 0,   &m_dwThreadID);
	if( m_HThread != NULL )
	{
		g_bRun = TRUE;
	}
	// Report running status when initialization is complete.
	ReportSvcStatus( SERVICE_RUNNING, NO_ERROR, 0 );
	bool bWriteFlag = false;
	while(1)
	{
		// Check whether to stop the service.

		WaitForSingleObject(ghSvcStopEvent, INFINITE);
		if(!bWriteFlag)
		{
			printf("Service normal stop: %s %s %d\r\n"
				, __FILE__, __FUNCTION__, __LINE__);
			bWriteFlag = true;
		}
		g_bRun = FALSE;
		ReportSvcStatus( SERVICE_STOPPED, NO_ERROR, 0 );
	}
}

//
// Purpose: 
//   Sets the current service status and reports it to the SCM.
//
// Parameters:
//   dwCurrentState - The current state (see SERVICE_STATUS)
//   dwWin32ExitCode - The system error code
//   dwWaitHint - Estimated time for pending operation, 
//     in milliseconds
// 
// Return value:
//   None
//
VOID WINSVC::ReportSvcStatus( DWORD dwCurrentState,
	DWORD dwWin32ExitCode,
	DWORD dwWaitHint)
{
	static DWORD dwCheckPoint = 1;

	// Fill in the SERVICE_STATUS structure.

	gSvcStatus.dwCurrentState = dwCurrentState;
	gSvcStatus.dwWin32ExitCode = dwWin32ExitCode;
	gSvcStatus.dwWaitHint = dwWaitHint;

	if (dwCurrentState == SERVICE_START_PENDING)
		gSvcStatus.dwControlsAccepted = 0;
	else gSvcStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP;

	if ( (dwCurrentState == SERVICE_RUNNING) ||
		(dwCurrentState == SERVICE_STOPPED) )
		gSvcStatus.dwCheckPoint = 0;
	else gSvcStatus.dwCheckPoint = dwCheckPoint++;

	// Report the status of the service to the SCM.
	SetServiceStatus( gSvcStatusHandle, &gSvcStatus );
}

//
// Purpose: 
//   Called by SCM whenever a control code is sent to the service
//   using the ControlService function.
//
// Parameters:
//   dwCtrl - control code
// 
// Return value:
//   None
//
VOID WINAPI WINSVC::SvcCtrlHandler( DWORD dwCtrl )
{
	// Handle the requested control code. 

	switch(dwCtrl) 
	{  
	case SERVICE_CONTROL_STOP: 
		ReportSvcStatus(SERVICE_STOP_PENDING, NO_ERROR, 0);

		// Signal the service to stop.

		SetEvent(ghSvcStopEvent);
		ReportSvcStatus(gSvcStatus.dwCurrentState, NO_ERROR, 0);

		return;

	case SERVICE_CONTROL_INTERROGATE: 
		break; 

	default: 
		break;
	} 

}

//
// Purpose: 
//   Logs messages to the event log
//
// Parameters:
//   szFunction - name of function that failed
// 
// Return value:
//   None
//
// Remarks:
//   The service must have an entry in the Application event log.
//
VOID WINSVC::SvcReportEvent(LPTSTR szFunction) 
{ 
	HANDLE hEventSource;
	LPCTSTR lpszStrings[2];
	TCHAR Buffer[80];

	hEventSource = RegisterEventSource(NULL, SVCNAME);

	if( NULL != hEventSource )
	{
		StringCchPrintf(Buffer, 80, TEXT("%s failed with %d, %s %s %d"), szFunction
			, GetLastError(), __FILE__, __FUNCTION__, __LINE__);

		lpszStrings[0] = SVCNAME;
		lpszStrings[1] = Buffer;

		ReportEvent(hEventSource,        // event log handle
			EVENTLOG_ERROR_TYPE, // event type
			0,                   // event category
			0,           // event identifier
			NULL,                // no security identifier
			2,                   // size of lpszStrings array
			0,                   // no binary data
			lpszStrings,         // array of strings
			NULL);               // no binary data

		DeregisterEventSource(hEventSource);
	}
}

VOID CALLBACK WINSVC::MainThread(HWND hwnd,UINT uMsg,UINT_PTR idEvent,DWORD dwTime)
{	
	int argc = 0;
	char * pArgv = NULL;
	printf("Main thread start successfully\r\n");
	MyMain(argc, &pArgv);
	while(WINSVC::g_bRun)
	{
		//code
		Sleep(100);
	}
	//MSG msg;
	//while (GetMessage(&msg, 0, 0, 0))
	//	DispatchMessage(&msg);
    printf("Main thread quit successfully!\r\n");
}
#endif
