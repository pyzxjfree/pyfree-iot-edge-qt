#ifndef MYFUNC_H
#define MYFUNC_H

#include <QString>

namespace PFunc
{
    int string2bytes(const char* pSrc, unsigned char* pDst, int nSrcLength);
    int bytes2string(const unsigned char* pSrc, char* pDst, int nSrcLength);
    unsigned int  crc16(unsigned char *ptr, unsigned int len);

    int code(const unsigned char *buff, const int len, unsigned char *outbuf);
    int uncode(const unsigned char *buff, int len, unsigned char *outbuf);

    bool ipCheck(QString ip_str);
    long ipToInt(QString ip_str);
    QString intToIp(long ip_int);

    ulong getIPLFromStr(QString _ipStr);
    QString getIPStrFromIPL(ulong _ipl);

    QString getCurrentTime();
};

#endif // MYFUNC_H
