﻿#include "hashmap.h"

#include "myFunc.h"

KeyObj_FT::KeyObj_FT(int _gid, int _id, PType _type)
	: m_gid(_gid), m_id(_id), m_type(_type)
{

};
//
int KeyObj_FT::cmp_Key(const KeyObj_FT &obj1, const KeyObj_FT &obj2)
{
	int diff = obj1.m_gid - obj2.m_gid;
	if (diff != 0) 		return diff;
	diff = (int)obj1.m_type - (int)obj2.m_type;
	if (diff != 0) 		return diff;
	diff = obj1.m_id - obj2.m_id;
	if (diff != 0) 		return diff;
	return 0;
};

//////////////////////////////////////////////

KeyObj_TF::KeyObj_TF(PType _pType, int _pID)
	: m_pType(_pType), m_pID(_pID)
{

};
//
int KeyObj_TF::cmp_Key(const KeyObj_TF &obj1, const KeyObj_TF &obj2)
{
	int diff = (int)obj1.m_pType - (int)obj2.m_pType;
	if (diff != 0) 		return diff;
	diff = obj1.m_pID - obj2.m_pID;
	if (diff != 0) 		return diff;
	return 0;
};


////////////////////////////////////////
KeyObj_Client::KeyObj_Client(QString  _ipStr, int _port)
	: m_ipStr(_ipStr), m_port(_port)
{
    m_ip = PFunc::getIPLFromStr(_ipStr);
};
//
long KeyObj_Client::cmp_Key(const KeyObj_Client &obj1, const KeyObj_Client &obj2)
{
	long diff = obj1.m_ip - obj2.m_ip;
	if (diff != 0) 		return diff;
	diff = obj1.m_port - obj2.m_port;
	if (diff != 0) 		return diff;
	return 0;
};
////////////////////////////////////////
KeyObj_Addr::KeyObj_Addr(int _addr, PType _type)
	: m_addr(_addr), m_ptype(_type)
{

};
//
int KeyObj_Addr::cmp_Key(const KeyObj_Addr &obj1, const KeyObj_Addr &obj2)
{
	int diff = obj1.m_addr - obj2.m_addr;
	if (diff != 0) 		return diff;
	diff = (int)obj1.m_ptype - (int)obj2.m_ptype;
	if (diff != 0) 		return diff;
	return 0;
};

/////////////////////////////

KeyObj_FTDev::KeyObj_FTDev(QString _ipStr, int _id, PType _type)
    : m_ipStr(_ipStr), m_id(_id), m_type(_type)
{
    m_ip = PFunc::ipToInt(_ipStr);
};
//
long KeyObj_FTDev::cmp_Key(const KeyObj_FTDev &obj1, const KeyObj_FTDev &obj2)
{
    long diff = static_cast<long>(obj1.m_ip - obj2.m_ip );
    if (diff != 0) 		return diff;
    diff = obj1.m_type - obj2.m_type;
    if (diff != 0) 		return diff;
    diff = obj1.m_id - obj2.m_id;
    if (diff != 0) 		return diff;
    return 0;
};

//////////////////////////////////////////////

KeyObj_TFDev::KeyObj_TFDev( long long _devID, int _pID)
    : m_devID(_devID), m_pID(_pID)
{

};
//
long long KeyObj_TFDev::cmp_Key(const KeyObj_TFDev &obj1, const KeyObj_TFDev &obj2)
{
    long long diff = obj1.m_devID - obj2.m_devID;
    if (diff != 0) 		return diff;
    diff = obj1.m_pID - obj2.m_pID;
    if (diff != 0) 		return diff;
    return 0;
};
