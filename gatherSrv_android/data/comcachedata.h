
#ifndef COMCACHEDATA_H
#define COMCACHEDATA_H

#include <QString>

class ComCacheData
{
public:
    static ComCacheData* getInstance();
	static void Destroy();
    ~ComCacheData();

private:
	void init();

    ComCacheData();
    ComCacheData(const ComCacheData& ) {} // copy constructor
    ComCacheData& operator=(const ComCacheData& ) { return *this; } // assignment operator
private:
    static ComCacheData* instance;
};

#endif
