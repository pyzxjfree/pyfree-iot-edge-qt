#ifndef RECEIVEDATA_H
#define RECEIVEDATA_H
/*数据中转缓存*/
#include <QQueue>
#include <QMap>
#include <QMutex>

#include "datadef.h"
#include "config/confdef.h"

struct JsonPValue
{
    int pID;
    int sec;
    int msec;
    float val;
};

class ReceiveData
{
public:
    static ReceiveData* getInstance();
    static void Destroy();
    ~ReceiveData();
    //点集
    //////////////////////////////////////////////////////////////
    int getRDSSize();
    bool isEmptyRDS();
    bool getFirstRDS(RDS &it);

    bool removeFirstRDS();
    void addRDS(RDS it);
    //////////////////////////////////////////////////////////////
    int getWDSSize();
    bool isEmptyWDS();
    bool getFirstWDS(WDS &it);

    bool removeFirstWDS();
    void addWDS(WDS it);
    //////////////////////////////////////////////////////////////
    //
    int getWDLCSize();
    bool isEmptyWDLC();
    bool getFirstWDLC(WDC &it);

    bool removeFirstWDLC();
    void addWDLC(WDC it);
    //////////////////////////////////////////////////////////////
    int getWDYunSize();
    bool isEmptyWDYun();
    bool getFirstWDYun(WDC &it);
    bool getFirstWDYunS(QMap<int,QQueue<JsonPValue> > &its,int size);

    bool removeFirstWDYun();
    void addWDYun(WDC it);
    //////////////////////////////////////////////////////////////
private:
    ReceiveData() {
        init();
    };
    ReceiveData& operator=(const ReceiveData&) {return *this;};
    void init() {
        OnDoIt_WDS = false;
        _ReadData_overS = 0;
        _WriteData_overS = 0;
        _WriteData_overLC = 0;
        _WriteData_overYun = 0;
    };
private:
    static ReceiveData* instance;
    /////////////////////////////点集转发////////////////////////////////////////////
    //协议解析结果缓存(各个转发端口或本地服务或远端服务写入数据,由管理线程读取处理,转到各个采集端口(串并口)处置)
    QQueue<RDS> ReadDataS;
    QMutex m_MutexRDS;
    //待写入转发口的数据缓存(各个采集端口写入数据,由管理线程读取处理,转到本地后台及转发端口)
    QQueue<WDS> WriteDataS;
    QMutex m_MutexWDS;
    bool OnDoIt_WDS;
    //本地客户端缓存(管理线程写入数据,本地后台服务读取处置）
    QQueue<WDC> WriteDataLC;
    QMutex m_MutexWDLC;
    //云端服务(金刚云信息发布系统)缓存(管理线程写入数据,ComXFSys中读取处置)
    QQueue<WDC> WriteDataYun;
    QMutex m_MutexWDYun;
private:
    static int QSize;
    //
    int _ReadData_overS;
    int _WriteData_overS;
    int _WriteData_overLC;
    int _WriteData_overYun;
};
#endif // RECEIVEDATA_H
