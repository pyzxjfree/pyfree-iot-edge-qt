#include "receivedata.h"

#include <QDebug>

int  ReceiveData::QSize = 100;
ReceiveData* ReceiveData::instance = NULL;
ReceiveData* ReceiveData::getInstance()
{
    if (NULL == ReceiveData::instance)
    {
        ReceiveData::instance = new ReceiveData();
    }
    return ReceiveData::instance;
}

void ReceiveData::Destroy()
{
    if (NULL != ReceiveData::instance) {
        delete ReceiveData::instance;
        ReceiveData::instance = NULL;
    }
}

ReceiveData::~ReceiveData()
{

}

//////////////////////////////////////////////////////////
int ReceiveData::getRDSSize()
{
    int size = 0;
    m_MutexRDS.lock();
    size = ReadDataS.size();
    m_MutexRDS.unlock();
    return size;
}

bool ReceiveData::isEmptyRDS()
{
    bool ret = false;
    m_MutexRDS.lock();
    ret = ReadDataS.empty();
    m_MutexRDS.unlock();
    return ret;
}

bool ReceiveData::getFirstRDS(RDS &it) {
    if (!isEmptyRDS()) {
        m_MutexRDS.lock();
        it = ReadDataS.front();
        m_MutexRDS.unlock();
        return true;
    }
    else {
        return false;
    }
}

bool ReceiveData::removeFirstRDS() {
    if (!isEmptyRDS()) {
        m_MutexRDS.lock();
        ReadDataS.pop_front();
        m_MutexRDS.unlock();
        return true;
    }
    else {
        return false;
    }
}

void ReceiveData::addRDS(RDS it) {
    if (getRDSSize() > QSize) {
        _ReadData_overS++;
        if (_ReadData_overS >= 10) {//每溢出10次，报告一次
            qWarning()<< QString("the size of ReceiveData::ReadDataS vector is up to limmit size: %1 .")
                       .arg(QSize);
            _ReadData_overS = 0;
        }
        removeFirstRDS();
    }
    m_MutexRDS.lock();
    ReadDataS.append(it);
    m_MutexRDS.unlock();
}

//////////////////////////////////////////////////////////
//
int ReceiveData::getWDSSize()
{
    int size = 0;
    m_MutexWDS.lock();
    size = WriteDataS.size();
    m_MutexWDS.unlock();
    return size;
}

bool ReceiveData::isEmptyWDS()
{
    bool ret = false;
    m_MutexWDS.lock();
    ret = WriteDataS.empty();
    m_MutexWDS.unlock();
    return ret;
}

bool ReceiveData::getFirstWDS(WDS &it) {
    if (!isEmptyWDS()) {
        m_MutexWDS.lock();
        it = WriteDataS.front();
        m_MutexWDS.unlock();
        return true;
    }
    else {
        return false;
    }
}

bool ReceiveData::removeFirstWDS() {
    if (!isEmptyWDS()) {
        m_MutexWDS.lock();
        WriteDataS.pop_front();
        m_MutexWDS.unlock();
        OnDoIt_WDS = true;
        return true;
    }
    else {
        return false;
    }
}

void ReceiveData::addWDS(WDS it) {
    if (getWDSSize() > QSize) {
        _WriteData_overS++;
        if (_WriteData_overS >= 10) {//每溢出10次，报告一次
            //正在处理中溢出才报告,无处理不做报告
            if (OnDoIt_WDS) {
                OnDoIt_WDS = false;
                qWarning()<< QString("the size of ReceiveData::WriteDataS vector is up to limmit size: %1 .")
                           .arg(QSize);
            }
            _WriteData_overS = 0;
        }
        removeFirstWDS();
    }
    m_MutexWDS.lock();
    WriteDataS.append(it);
    m_MutexWDS.unlock();
}
/////////////////////////////////////////////////////////////////////////////////////////////////
//
int ReceiveData::getWDLCSize()
{
    int size = 0;
    m_MutexWDLC.lock();
    size = WriteDataLC.size();
    m_MutexWDLC.unlock();
    return size;
}

bool ReceiveData::isEmptyWDLC()
{
    bool ret = false;
    m_MutexWDLC.lock();
    ret = WriteDataLC.empty();
    m_MutexWDLC.unlock();
    return ret;
}

bool ReceiveData::getFirstWDLC(WDC &it) {
    if (!isEmptyWDLC()) {
        m_MutexWDLC.lock();
        it = WriteDataLC.front();
        m_MutexWDLC.unlock();
        return true;
    }
    else {
        return false;
    }
}

bool ReceiveData::removeFirstWDLC() {
    if (!isEmptyWDLC()) {
        m_MutexWDLC.lock();
        WriteDataLC.pop_front();
        m_MutexWDLC.unlock();
        return true;
    }
    else {
        return false;
    }
}

void ReceiveData::addWDLC(WDC it) {
    if (getWDLCSize() > QSize) {
        _WriteData_overLC++;
        if (_WriteData_overLC >= 100) {//每溢出100次，报告一次
            qWarning()<< QString("the size of ReceiveData::WriteDataLC vector is up to limmit size: %1 .")
                       .arg(QSize);
            _WriteData_overLC = 0;
        }
        removeFirstWDLC();
    }
    m_MutexWDLC.lock();
    WriteDataLC.append(it);
    m_MutexWDLC.unlock();
}
/////////////////////////////////////////////////////////////////////////////////////////////////
//
int ReceiveData::getWDYunSize()
{
    int size = 0;
    m_MutexWDYun.lock();
    size = WriteDataYun.size();
    m_MutexWDYun.unlock();
    return size;
}

//
bool ReceiveData::isEmptyWDYun()
{
    bool ret = true;
    m_MutexWDYun.lock();
    ret = WriteDataYun.empty();
    m_MutexWDYun.unlock();
    return ret;
}

bool ReceiveData::getFirstWDYun(WDC &it) {
    if (!isEmptyWDYun()) {
        m_MutexWDYun.lock();
        it = WriteDataYun.front();
        m_MutexWDYun.unlock();
        return true;
    }
    else {
        return false;
    }
}

bool ReceiveData::getFirstWDYunS(QMap<int,QQueue<JsonPValue> > &its,int size)
{
    if (!isEmptyWDYun()) {
        int rsize = getWDYunSize();
        rsize = rsize<size?rsize:size;
        m_MutexWDYun.lock();
        for(int i=0;i<rsize;i++){
            WDC it = WriteDataYun.dequeue();
            QMap<int,QQueue<JsonPValue> >::iterator itpos = its.find(it.devID);
            if(itpos != its.end()){
                JsonPValue jval;
                jval.pID = it.pID;
                jval.sec = it.evtTimeS;
                jval.msec = it.evtTimeMS;
                jval.val = it.val;
                its[it.devID].append(jval);
            }else{
                QQueue<JsonPValue> pids;
                JsonPValue jval;
                jval.pID = it.pID;
                jval.sec = it.evtTimeS;
                jval.msec = it.evtTimeMS;
                jval.val = it.val;
                pids.append(jval);
                its[it.devID] = pids;
            }
        }
        m_MutexWDYun.unlock();
        return its.size();
    }
    else {
        return false;
    }
}

bool ReceiveData::removeFirstWDYun() {
    if (!isEmptyWDYun()) {
        m_MutexWDYun.lock();
        WriteDataYun.pop_front();
        m_MutexWDYun.unlock();
        return true;
    }
    else {
        return false;
    }
}

void ReceiveData::addWDYun(WDC it) {
    if (getWDYunSize() > QSize) {
        _WriteData_overYun++;
        if (_WriteData_overYun >= 100) {//每溢出100次，报告一次
            qWarning()<< QString("the size of ReceiveData::WriteDataYun vector is up to limmit size: %1 .")
                       .arg(QSize);
            _WriteData_overYun = 0;
        }
        removeFirstWDYun();
    }
    m_MutexWDYun.lock();
    WriteDataYun.append(it);
    m_MutexWDYun.unlock();
}
