#include "comcachedata.h"

ComCacheData* ComCacheData::instance = NULL;
ComCacheData* ComCacheData::getInstance()
{
    if(NULL == ComCacheData::instance)
    {
        ComCacheData::instance = new ComCacheData();
    }
    return ComCacheData::instance;
}

void ComCacheData::Destroy()
{
    if(NULL!=ComCacheData::instance){
        delete ComCacheData::instance;
        ComCacheData::instance = NULL;
	}
}

ComCacheData::ComCacheData()
{
	init();
}

ComCacheData::~ComCacheData()
{
}

void ComCacheData::init()
{

}
