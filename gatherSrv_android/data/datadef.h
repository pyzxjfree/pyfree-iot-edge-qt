#ifndef DATADEF_H
#define DATADEF_H

#include <QList>
#include <QString>
#include <QDateTime>

#include "config/confdef.h"

struct CMDLib
{
    CMDLib()
        : time_interval(1000)
    {

    };

    ulong time_interval;
    QList<QString> cmd_totalcall;
};

//enum ExeType
//{
//    OnGet = 1 ,//查询
//    OnSet = 2 ,//设值
//    OnTran= 3 ,//转发
//    ExeTDef = 0
//};

struct RDS
{
    RDS()
        : exeType(ExeTDef)
        , pID(0)
        , pType(PTDef)
        , val(0.0)
        , taskID(0)
    {

    };
    RDS(ExeType	_exeType
        , unsigned int _pID
        , PType _pType
        , float _val
        , unsigned long _taskID=0)
        : exeType(_exeType)
        , pID(_pID)
        , pType(_pType)
        , val(_val)
        , taskID(_taskID)
    {
    };
    RDS& operator=(const RDS &rval)
    {
        if (this == &rval) {
            return *this;
        }
        pID = rval.pID;
        exeType = rval.exeType;
        pType = rval.pType;
        val = rval.val;
        taskID = rval.taskID;
        return *this;
    };
    ExeType	exeType;
    unsigned int pID;
    PType	pType;
    //unsigned int evtTimeS;
    //unsigned int evtTimeMS;
    float val;
    unsigned long taskID;
};

struct WDS
{
    WDS()
        : pType(PTDef)
        , pID(0)
        , /*evtTimeS(((unsigned int)time(NULL))), evtTimeMS(0),*/ val(0.0)
        , upChangeF(false)
        , taskID(0)
    {
    };
    WDS(PType _pType, unsigned int _pID
        //, unsigned int _s, unsigned int _ms
        , float _val,bool _upChangeF=false,unsigned long _taskID=0)
    {
        pType = _pType;
        pID = _pID;
        //evtTimeS = _s;
        //evtTimeMS = _ms;
        val = _val;
        upChangeF = _upChangeF;
        taskID = _taskID;
    };
    WDS& operator=(const WDS &rval)
    {
        if (this == &rval) {
            return *this;
        }
        pType = rval.pType;
        pID = rval.pID;
        val = rval.val;
        //evtTimeS = rval.evtTimeS;
        //evtTimeMS = rval.evtTimeMS;
        upChangeF = rval.upChangeF;
        taskID = rval.taskID;
        return *this;
    };
    ////////
    PType pType;
    unsigned int pID;
    //unsigned int evtTimeS;
    //unsigned int evtTimeMS;
    float val;
    bool upChangeF;
    unsigned long taskID;
};

struct WDC
{
    WDC()
        : devID(0)
        , pID(0)
        , evtTimeS(QDateTime::currentSecsSinceEpoch())
        , evtTimeMS(0)
        , val(0.0)
        , clientType(1)
        , len(0)
    {
        memset(Buf, 0, 512);
    };
    WDC(unsigned long long _devID, unsigned int _pID
        , unsigned int _s, unsigned int _ms, float _val,int _clientType=1)
    {
        devID = _devID;
        pID = _pID;
        evtTimeS = _s;
        evtTimeMS = _ms;
        val = _val;
        clientType = _clientType;
        len = 0;
        memset(Buf, 0, 512);
    };
    WDC(unsigned long long _devID, unsigned int _pID
        , unsigned int _s, unsigned int _ms, float _val, int _clientType
        , unsigned char *buf, int nlen)
    {
        devID = _devID;
        pID = _pID;
        evtTimeS = _s;
        evtTimeMS = _ms;
        val = _val;
        clientType = _clientType;
        memset(Buf, 0, 512);
        memcpy(Buf, buf, nlen);
        len = nlen;
    };
    WDC& operator=(const WDC &rval)
    {
        if (this == &rval) {
            return *this;
        }
        devID = rval.devID;
        pID = rval.pID;
        val = rval.val;
        clientType = rval.clientType;
        evtTimeS = rval.evtTimeS;
        evtTimeMS = rval.evtTimeMS;

        memset(Buf, 0, 512);
        if (rval.len <= 512) {
            memcpy(Buf, rval.Buf, rval.len);
            len = rval.len;
        }
        else {
            memcpy(Buf, rval.Buf, 512);
            len = 512;
        }

        return *this;
    };
    ////////
    unsigned long long devID;
    unsigned int	pID;
    unsigned int	evtTimeS;
    unsigned int	evtTimeMS;
    float			val;
    int				clientType;
    unsigned char Buf[512];
    unsigned int len;
};

struct WDSPCS
{
    WDSPCS() : ipInt(0), exeType(ExeTDef)
        , pID(0), pType(PTDef), val(0.0), sleep(0), desc("")
    {

    };
    WDSPCS(unsigned long long _ipInt, ExeType	_exeType
        , unsigned int _pID, PType _pType, float _val)
        : ipInt(_ipInt), exeType(_exeType)
        , pID(_pID), pType(_pType), val(_val)
        , sleep(0), desc("")
    {
    };
    WDSPCS(unsigned long long _ipInt, ExeType	_exeType
           , unsigned int _pID, PType _pType, float _val
           , qint64 _sleep,QString _desc)
        : ipInt(_ipInt), exeType(_exeType)
        , pID(_pID), pType(_pType), val(_val)
        , sleep(_sleep), desc(_desc)
    {
    };

    WDSPCS& operator=(const WDSPCS &rval)
    {
        if (this == &rval) {
            return *this;
        }
        ipInt = rval.ipInt;
        pID = rval.pID;
        exeType = rval.exeType;
        pType = rval.pType;
        val = rval.val;
        return *this;
    };
    unsigned long long ipInt;
    ExeType	exeType;
    unsigned int pID;
    PType	pType;
    //unsigned int evtTimeS;
    //unsigned int evtTimeMS;
    float val;
    qint64 sleep;
    QString desc;
};

struct NetArg
{
    QString ipStr;  //网络地址字符串
    int     port;   //端口
    int     type;   //数据读写类型
    int     ptype;  //协议解析类型
    bool    heartFlag;
    int     timePush;
};

struct RD
{
    RD() : len(0)
    {
        memset(Buf, 0, 512);
    };
    RD(unsigned char *buf, int nlen)
    {
        memset(Buf, 0, 512);
        memcpy(Buf, buf, nlen);
        len = nlen;
    };
    RD& operator=(const RD &rval)
    {
        if (this != &rval) {
            memset(Buf, 0, 512);
            if (rval.len <= 512) {
                memcpy(Buf, rval.Buf, rval.len);
                len = rval.len;
            }
            else {
                memcpy(Buf, rval.Buf, 512);
                len = 512;
            }
        }
        return *this;
    };
    unsigned char Buf[512];
    int len;
};

#define RDCSIZE 1024
struct RDClient
{
    RDClient()
        : len(0)
    {
        memset(Buf, 0, RDCSIZE);
    };
    RDClient(unsigned char *buf, int nlen)
    {
        memset(Buf, 0, RDCSIZE);
        memcpy(Buf, buf, nlen);
        len = nlen;
    };
    ~RDClient()
    {
    };
    RDClient& operator=(const RDClient &rval)
    {
        if (this != &rval)
        {
            memset(Buf, 0, RDCSIZE);
            memcpy(Buf, rval.Buf, rval.len);
            len = rval.len;
        }
        return *this;
    };
    int add(unsigned char *buf, int nlen)
    {
        try {
            memset(Buf + len, 0, RDCSIZE - len);
            memcpy(Buf + len, buf, nlen);
            len += nlen;
        }
        catch (...)
        {
            printf("RDClient::add error \r\n");
        }
        return len;
    };
    unsigned char Buf[RDCSIZE];
    int len;
};

#endif // DATADEF_H
