﻿#pragma once
#ifndef HASH_MAP_H
#define HASH_MAP_H

#include <QString>
#include <QMap>

#include "config/confdef.h"

class KeyObj_FT
{
public:
	KeyObj_FT(int _gid, int _id, PType _type);
	//
	static int cmp_Key(const KeyObj_FT &obj1, const KeyObj_FT &obj2);

	int m_gid;
	int	m_id;
	PType m_type;
private:
};
inline bool operator==(const KeyObj_FT& obj1, const KeyObj_FT& obj2) { return KeyObj_FT::cmp_Key(obj1, obj2) == 0; }
inline bool operator!=(const KeyObj_FT& obj1, const KeyObj_FT& obj2) { return KeyObj_FT::cmp_Key(obj1, obj2) != 0; }
inline bool operator>=(const KeyObj_FT& obj1, const KeyObj_FT& obj2) { return KeyObj_FT::cmp_Key(obj1, obj2) >= 0; }
inline bool operator<=(const KeyObj_FT& obj1, const KeyObj_FT& obj2) { return KeyObj_FT::cmp_Key(obj1, obj2) <= 0; }
inline bool operator>(const KeyObj_FT& obj1, const KeyObj_FT& obj2) { return KeyObj_FT::cmp_Key(obj1, obj2) > 0; }
inline bool operator<(const KeyObj_FT& obj1, const KeyObj_FT& obj2) { return KeyObj_FT::cmp_Key(obj1, obj2) < 0; }
////////////////////////////////////////////////////////////////////////
template <class T>
class MyObj_FT
{
public:
	MyObj_FT();
	void insert(KeyObj_FT key, T val);
	bool getVal(KeyObj_FT key, T &_val);
	int size();
	//其他函数类似实现
private:
    QMap<KeyObj_FT, T> data;
};
////////////////////////////////////////////////
template <class T>
MyObj_FT<T>::MyObj_FT()
{
};

template <class T>
void MyObj_FT<T>::insert(KeyObj_FT key, T val)
{
	data[key] = val;
};

template <class T>
bool MyObj_FT<T>::getVal(KeyObj_FT key, T &_val)
{
    typename QMap<KeyObj_FT,T>::iterator it = data.find(key);
	if (it != data.end()) {
        _val = it.value();
		return true;
	}
	return false;
};
template <class T>
int MyObj_FT<T>::size()
{
	return static_cast<int>(data.size());
};

//////////////////////////////////////////////////////////
class KeyObj_TF
{
public:
	KeyObj_TF(PType _pType, int _pID);
	//
	static int cmp_Key(const KeyObj_TF &obj1, const KeyObj_TF &obj2);

	PType	m_pType;
	int m_pID;
private:

};
inline bool operator==(const KeyObj_TF& obj1, const KeyObj_TF& obj2) { return KeyObj_TF::cmp_Key(obj1, obj2) == 0; }
inline bool operator!=(const KeyObj_TF& obj1, const KeyObj_TF& obj2) { return KeyObj_TF::cmp_Key(obj1, obj2) != 0; }
inline bool operator>=(const KeyObj_TF& obj1, const KeyObj_TF& obj2) { return KeyObj_TF::cmp_Key(obj1, obj2) >= 0; }
inline bool operator<=(const KeyObj_TF& obj1, const KeyObj_TF& obj2) { return KeyObj_TF::cmp_Key(obj1, obj2) <= 0; }
inline bool operator>(const KeyObj_TF& obj1, const KeyObj_TF& obj2) { return KeyObj_TF::cmp_Key(obj1, obj2) > 0; }
inline bool operator<(const KeyObj_TF& obj1, const KeyObj_TF& obj2) { return KeyObj_TF::cmp_Key(obj1, obj2) < 0; }

template <class T>
class MyObj_TF
{
public:
	MyObj_TF();
	void insert(KeyObj_TF key, T val);
	bool getVal(KeyObj_TF key,T &_val);
	int size();
	//其他函数类似实现
private:
    QMap<KeyObj_TF, T> data;
};
////////////////////////////////////////////
template <class T>
MyObj_TF<T>::MyObj_TF()
{
};

template <class T>
void MyObj_TF<T>::insert(KeyObj_TF key, T val)
{
	data[key] = val;
};

template <class T>
bool MyObj_TF<T>::getVal(KeyObj_TF key,T &_val)
{
    typename QMap<KeyObj_TF, T>::iterator it = data.find(key);
	if (it != data.end()) {
        _val = it.value();
		return true;
	}
	return false;
};
template <class T>
int MyObj_TF<T>::size()
{
	return static_cast<int>(data.size());
};

//////////////////////////////////////
class KeyObj_Client
{
public:
    KeyObj_Client(QString  _ipStr, int _port);
	//
	static long cmp_Key(const KeyObj_Client &obj1, const KeyObj_Client &obj2);

    QString m_ipStr;
    int	m_port;
    long	m_ip;
private:
};
inline bool operator==(const KeyObj_Client& obj1, const KeyObj_Client& obj2) { return KeyObj_Client::cmp_Key(obj1, obj2) == 0; }
inline bool operator!=(const KeyObj_Client& obj1, const KeyObj_Client& obj2) { return KeyObj_Client::cmp_Key(obj1, obj2) != 0; }
inline bool operator>=(const KeyObj_Client& obj1, const KeyObj_Client& obj2) { return KeyObj_Client::cmp_Key(obj1, obj2) >= 0; }
inline bool operator<=(const KeyObj_Client& obj1, const KeyObj_Client& obj2) { return KeyObj_Client::cmp_Key(obj1, obj2) <= 0; }
inline bool operator>(const KeyObj_Client& obj1, const KeyObj_Client& obj2) { return KeyObj_Client::cmp_Key(obj1, obj2) > 0; }
inline bool operator<(const KeyObj_Client& obj1, const KeyObj_Client& obj2) { return KeyObj_Client::cmp_Key(obj1, obj2) < 0; }

//////////////////////////////////////
class KeyObj_Addr
{
public:
	KeyObj_Addr(int _addr, PType _type);
	//
	static int cmp_Key(const KeyObj_Addr &obj1, const KeyObj_Addr &obj2);

	int	m_addr;
	int	m_ptype;
private:
};
inline bool operator==(const KeyObj_Addr& obj1, const KeyObj_Addr& obj2) { return KeyObj_Addr::cmp_Key(obj1, obj2) == 0; }
inline bool operator!=(const KeyObj_Addr& obj1, const KeyObj_Addr& obj2) { return KeyObj_Addr::cmp_Key(obj1, obj2) != 0; }
inline bool operator>=(const KeyObj_Addr& obj1, const KeyObj_Addr& obj2) { return KeyObj_Addr::cmp_Key(obj1, obj2) >= 0; }
inline bool operator<=(const KeyObj_Addr& obj1, const KeyObj_Addr& obj2) { return KeyObj_Addr::cmp_Key(obj1, obj2) <= 0; }
inline bool operator>(const KeyObj_Addr& obj1, const KeyObj_Addr& obj2) { return KeyObj_Addr::cmp_Key(obj1, obj2) > 0; }
inline bool operator<(const KeyObj_Addr& obj1, const KeyObj_Addr& obj2) { return KeyObj_Addr::cmp_Key(obj1, obj2) < 0; }

//

class KeyObj_FTDev
{
public:
    KeyObj_FTDev(QString  _ipStr, int _id, PType _type);
    //
    static long cmp_Key(const KeyObj_FTDev &obj1, const KeyObj_FTDev &obj2);

    QString m_ipStr;
    unsigned long	m_ip;
    int	m_id;
    PType m_type; 
private:
};

inline bool operator==(const KeyObj_FTDev& obj1, const KeyObj_FTDev& obj2) { return KeyObj_FTDev::cmp_Key(obj1, obj2) == 0; }
inline bool operator!=(const KeyObj_FTDev& obj1, const KeyObj_FTDev& obj2) { return KeyObj_FTDev::cmp_Key(obj1, obj2) != 0; }
inline bool operator>=(const KeyObj_FTDev& obj1, const KeyObj_FTDev& obj2) { return KeyObj_FTDev::cmp_Key(obj1, obj2) >= 0; }
inline bool operator<=(const KeyObj_FTDev& obj1, const KeyObj_FTDev& obj2) { return KeyObj_FTDev::cmp_Key(obj1, obj2) <= 0; }
inline bool operator>(const KeyObj_FTDev& obj1, const KeyObj_FTDev& obj2) { return KeyObj_FTDev::cmp_Key(obj1, obj2) > 0; }
inline bool operator<(const KeyObj_FTDev& obj1, const KeyObj_FTDev& obj2) { return KeyObj_FTDev::cmp_Key(obj1, obj2) < 0; }

////////////////////////////////////////////////////////////////////////
template <class T>
class MyObj_FTDev
{
public:
    MyObj_FTDev();
    void insert(KeyObj_FTDev key, T val);
    bool getVal(KeyObj_FTDev key, T &_val);
    int size();
    //其他函数类似实现
private:
    std::map<KeyObj_FTDev, T> data;
};
////////////////////////////////////////////////
template <class T>
MyObj_FTDev<T>::MyObj_FTDev() {};

template <class T>
void MyObj_FTDev<T>::insert(KeyObj_FTDev key, T val)
{
    data[key] = val;
};

template <class T>
bool MyObj_FTDev<T>::getVal(KeyObj_FTDev key, T &_val)
{
    typename std::map<KeyObj_FTDev, T>::iterator it = data.find(key);
    if (it != data.end()) {
        _val = it->second;
        return true;
    }
    return false;
};
template <class T>
int MyObj_FTDev<T>::size()
{
    return static_cast<int>(data.size());
};

//////////////////////////////////////////////////////////
class KeyObj_TFDev
{
public:
    KeyObj_TFDev(long long _devID, int _pID);
    //
    static long long cmp_Key(const KeyObj_TFDev &obj1, const KeyObj_TFDev &obj2);

    long long	m_devID;
    int m_pID;
private:

};
inline bool operator==(const KeyObj_TFDev& obj1, const KeyObj_TFDev& obj2) { return KeyObj_TFDev::cmp_Key(obj1, obj2) == 0; }
inline bool operator!=(const KeyObj_TFDev& obj1, const KeyObj_TFDev& obj2) { return KeyObj_TFDev::cmp_Key(obj1, obj2) != 0; }
inline bool operator>=(const KeyObj_TFDev& obj1, const KeyObj_TFDev& obj2) { return KeyObj_TFDev::cmp_Key(obj1, obj2) >= 0; }
inline bool operator<=(const KeyObj_TFDev& obj1, const KeyObj_TFDev& obj2) { return KeyObj_TFDev::cmp_Key(obj1, obj2) <= 0; }
inline bool operator>(const KeyObj_TFDev& obj1, const KeyObj_TFDev& obj2) { return KeyObj_TFDev::cmp_Key(obj1, obj2) > 0; }
inline bool operator<(const KeyObj_TFDev& obj1, const KeyObj_TFDev& obj2) { return KeyObj_TFDev::cmp_Key(obj1, obj2) < 0; }

//////////////////

template <class T>
class MyObj_TFDev
{
public:
    MyObj_TFDev();
    void insert(KeyObj_TFDev key, T val);
    bool getVal(KeyObj_TFDev key,T &_val);
    int size();
    //其他函数类似实现
private:
    std::map<KeyObj_TFDev, T> data;
};
////////////////////////////////////////////
template <class T>
MyObj_TFDev<T>::MyObj_TFDev() {};

template <class T>
void MyObj_TFDev<T>::insert(KeyObj_TFDev key, T val)
{
    data[key] = val;
};

template <class T>
bool MyObj_TFDev<T>::getVal(KeyObj_TFDev key,T &_val)
{
    typename std::map<KeyObj_TFDev, T>::iterator it = data.find(key);
    if (it != data.end()) {
        _val = it->second;
        return true;
    }
    return false;
};
template <class T>
int MyObj_TFDev<T>::size()
{
    return static_cast<int>(data.size());
};

#endif //HASH_MAP_H

