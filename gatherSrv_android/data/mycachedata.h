#ifndef MYCACHEDATA_H
#define MYCACHEDATA_H

#include <QObject>
#include <QMutex>

#include "config/confdef.h"
#include "data/hashmap.h"

QT_BEGIN_NAMESPACE
class ReceiveData;
QT_END_NAMESPACE

struct DevClient{
    int devID;
    int devType;
    QString name;
};

struct PointClient{
    int pID;
    int pType;
    QString name;
    float defval;
};

class MyCacheData : public QObject
{
    Q_OBJECT
public:
    static MyCacheData* getInstance();
    static void Destroy();
    ~MyCacheData();

    bool getPTO(int _gid, int _pid, PType _ptype, PTo &_pto);
    bool getPFrom(int _pid, PType _ptype,PFrom &pf);

    bool setValue(QString _ipStr, int _id, PType _pType, float _val);
    bool setValue(unsigned long long _devID, unsigned int _pID
        , float &_val,bool &sendflag, bool &recordflag,int &clientType);
    bool getConditionJude(unsigned long long _devID, unsigned int _pID, float rVal, float &_val, bool &_valChange);
    //获取值
    bool getValue(unsigned long long _devID, unsigned int _pID, float &_val);
    //计算值
    bool getCValue(unsigned long long _devID, unsigned int _pID, float &_val);
    //
    qint64 getUsec();
    qint64 getSec();

    QList<Plan> getPlans();
    bool getFromInfo(unsigned long long _devID, unsigned int _pID,PFromDev &_pfrom);

    QList<DevClient> getDevInfo();
    QList<PointClient> getPointInfo(int devID);

    void TimeUpVirtualPInfo();
public:
    //json
    QString getIpClientJson()
    {
        return appConf.ipCleintJson;
    };

    int getPortClientJson()
    {
        return appConf.portClientJson;
    };
    int getPortLocal(){
        return appConf.portLocal;
    };

    //disk
    char getDiskSymbol()
    {
        return appConf.diskSymbol;
    };

    int getFreeSizeLimit()
    {
        return appConf.freeSizeLimit;
    };
    //area info
    int getAreaId(){
        return appConf.areaId;
    };
    int getAreaType(){
        return appConf.areaType;
    };
    QString getAreaName(){
        return appConf.areaName;
    };
    QString getAreaDesc(){
        return appConf.areaDesc;
    };
    //func switch
    int getYunFunc(){
        return appConf.yunFunc;
    };
    int getMcsFunc(){
        return appConf.mcsFunc;
    };
    int getTUpVirtualFunc(){
        return appConf.timeUpVirtual;
    };
    int getViewFunc(){
        return appConf.viewFunc;
    };

private:
    MyCacheData(QObject *parent = Q_NULLPTR)
        : QObject(parent)
    {
        init();
    };
    MyCacheData& operator=(const MyCacheData&) {return *this;};
    void init();
//signals:
//    void readLod(QString _log);
public:
    ComManagerDef commdef;
private:
    static MyCacheData* instance;
    static qreal FLIMIT;
    ReceiveData *ptr_ReceiveData;

    MyConf appConf;
    CalculateIDS ids;
    QList<Dev> mydevs;
    QMutex *devs_mutex;
    QList<PMapDev> pmapsDev;
    QList<Plan> plans;

//    ComManagerDef commdef;
    QList<PMap> pmaps;
    //
    MyObj_FTDev<PToDev> ftmapDev;
    MyObj_TFDev<PFromDev> tfmapDev;
    //
    MyObj_FT<PTo> ftmap;
    MyObj_TF<PFrom> tfmap;
};

#endif // MYCACHEDATA_H
