<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>GatherMgr</name>
    <message>
        <location filename="../src/gathermgr.cpp" line="352"/>
        <location filename="../src/gathermgr.cpp" line="353"/>
        <source>sye-pcs-gathercontrol</source>
        <translation type="unfinished">兴业调光投影采集中控</translation>
    </message>
    <message>
        <location filename="../src/gathermgr.cpp" line="354"/>
        <source>sye-pcs-gathercontrol-manage</source>
        <translation type="unfinished">兴业调光投影控制管理</translation>
    </message>
    <message>
        <location filename="../src/gathermgr.cpp" line="363"/>
        <source>Min(&amp;I)</source>
        <translation type="unfinished">隐藏(&amp;I)</translation>
    </message>
    <message>
        <location filename="../src/gathermgr.cpp" line="365"/>
        <source>Rec(&amp;R)</source>
        <translation type="unfinished">显示(&amp;R)</translation>
    </message>
    <message>
        <location filename="../src/gathermgr.cpp" line="372"/>
        <source>Quit(&amp;Q)</source>
        <translation type="unfinished">退出(&amp;Q)</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../serialport/serialport.cpp" line="260"/>
        <location filename="../serialport/serialport.cpp" line="275"/>
        <location filename="../serialport/serialport.h" line="49"/>
        <location filename="../serialport/serialport.h" line="53"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../serialport/serialport.cpp" line="263"/>
        <source>Even</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../serialport/serialport.cpp" line="266"/>
        <source>Odd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../serialport/serialport.cpp" line="269"/>
        <source>Mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../serialport/serialport.cpp" line="272"/>
        <source>Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../serialport/serialport.cpp" line="537"/>
        <source>Open error,ret(%1),Index(%2)</source>
        <translation type="unfinished">打开错误,返回(%1),索引(%2)</translation>
    </message>
</context>
<context>
    <name>SerialPort</name>
    <message>
        <location filename="../serialport/serialport.cpp" line="496"/>
        <source>Critical Error</source>
        <translation type="unfinished">串口通信严重错误</translation>
    </message>
    <message>
        <location filename="../serialport/serialport.cpp" line="512"/>
        <source>Connected to %1 : %2, %3, %4, %5, %6</source>
        <translation type="unfinished">链接到%1:%2,%3,%4,%5,%6</translation>
    </message>
    <message>
        <location filename="../serialport/serialport.cpp" line="520"/>
        <source>Open %1 Error</source>
        <translation type="unfinished">打开%1错误</translation>
    </message>
    <message>
        <location filename="../serialport/serialport.cpp" line="550"/>
        <source>Disconnected</source>
        <translation type="unfinished">断开链接</translation>
    </message>
</context>
</TS>
