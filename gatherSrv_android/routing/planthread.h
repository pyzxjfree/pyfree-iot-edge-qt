#ifndef PLANTHREAD_H
#define PLANTHREAD_H


#include <QThread>
#include <QMutex>

#include "config/confdef.h"
#include "data/datadef.h"

QT_BEGIN_NAMESPACE
class MyCacheData;
class ReceiveData;
QT_END_NAMESPACE

class PlanThread : public QThread
{
    Q_OBJECT
public:
    PlanThread(QObject *parent = Q_NULLPTR);
    ~PlanThread();
protected:
    virtual void run();
private:
    qint64 getUsec();
    qint64 getSec();

    bool mapCurTime( PlanTime &_pt);
    bool inWorkTime(unsigned int min_, QList<TVProperty> timeVS);
    bool mapSleepTime(QList<TVProperty> timeVS,PlanSleep &_ps);
    //
    bool conditionJude(QList<TVProperty> timeVS,QList<PlanCondition> _plancons);
    bool compareVal(float _fv, float _tv, CompareType _ct);
    //
    void doCmd(QList<PlanCMD> planCmds);
private:
    bool running;
    MyCacheData *mycdata;
    ReceiveData *m_ReceiveData;
    QList<Plan> plans;
    unsigned int timePlan;

    QList<WDSPCS> WriteDataS;
    QMutex m_MutexWDS;
};

#endif // PLANTHREAD_H
