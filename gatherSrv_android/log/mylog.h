#ifndef MYLOG_H
#define MYLOG_H
/*
 * 2018-01-23 modify, the func of log from main.cpp create a new codefile do it.
 * log can be record day by day, and one day will be do it for some files when file size is over limit.
 */
#include <QString>

//the size unit is B,100000B is 100KB
#define LOGFILEMAX 1000000

namespace AppLog {
    void logMsgHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);
    QString getLogFile();
    QString getLogDir();
    void initLog();
}

#endif // MYLOG_H
