#ifndef LOGMONITOR_H
#define LOGMONITOR_H
/*
 * 2018-01-24 add for remove the old log
 * when the disk free size is limit'size
 * which be set from appconf.xml
 */
#include <QThread>

class LogMonitor : public QThread
{
    Q_OBJECT
public:
    LogMonitor(QObject *parent = Q_NULLPTR);
    virtual ~LogMonitor();
protected:
    virtual void run();
private:
    void diskInfo(QString name, QString val);
    void freeLog();
private:
    bool running;
    int doTime;
    int limitSize;
    QString logDir;
};
#endif // LOGMONITOR_H
