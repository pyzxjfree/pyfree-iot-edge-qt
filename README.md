# pyfree-IotEdge-Qt

#### 介绍
边缘采集服务，类似pyfree-IotEdge，进行功能裁减，采用QT开发，支持android

#### 软件架构
软件架构说明


#### 安装教程

1.  见doc目录

#### 使用说明

1.  本文采用QT5.8的静态编译版本及qtcreater编译工具,在qtcreater界面的项目可重新设置构建输出目录
2.	GatherControl文件包含软件运行示例配置文件，可以将编译输出程序拷贝到该目录运行测试，亦可将配置文件拷贝到程序输出目录，配置逻辑及意义见pyfree-IotEdge。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
