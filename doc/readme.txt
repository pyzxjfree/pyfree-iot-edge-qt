关于软件结构类似于pyfree-IotEdge,将其主要采集、调度、转发等功能采用QT集在一个控制软件实现，支持android系统
编译支持：
1）Win64 	qt5.8_for_win
2) linux64  	qt5.8_for_linux
3) android 	qt5.8_for_android

模块描述（gatherSrv_android目录下）：
1）android：	android配置文件
2）config：	配置参数结构及配置文件读取
3）data：	结构数据、缓存数据及中转数据类集
4）images：	图片资源
5) javaSrc:	c++与java交互接口
6）languages：	语言翻译配置
7）log：	日志模块
8）netport：	网口数据采集实现模块
9) routing:	任务计划
10）script：	java脚本调用接口，qt版本里，采用js脚本实现规约解析的
11) SerialPort:	串口数据采集实现模块
12）src：	主程序模块
13）config：	配置参数结构及配置文件读取
14）svc_common:	win服务化安装、卸载代码
15）transport：	数据转换功能，包括与信息发布系统的通信接口、与监控服务的通信接口、与中控终端客户端的通信接口、定期推送监控数据模块

辅助：
usb-permission-issuer.zip工具用于支持USB热插拔忽略确定对话框
1）usb-permission-issuer.apk,需要将其拷贝到android系统下的/system/prv-app目录下（该目录是系统预装apk路径），并修改其文件权限，例如777

2）参考做法，直接下载了“kingroot”和“RE文件管理”，安装后，使用“kingroot”给“RE文件管理”开启root权限，然后通过“RE文件管理”将“usb-permission-issuer.apk”拷贝到/system/prv-app目录下，并修改其文件权限（选择文件长按，RE文件管理会出现更多功能菜单，里面有权限修改）